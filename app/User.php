<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Caffeinated\Shinobi\Traits\ShinobiTrait;

class User extends Authenticatable
{
    use Notifiable, ShinobiTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'jabatan', 'password', 'api_token', 'probing_point', 'deal_point'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    public function RoleUser() {
        return $this->belongsTo('App\Models\Auth\Role_user','id','user_id');
    }

    public function Pelanggan() {
        return $this->hasMany('App\Models\Master\DataPelanggan','id','created_by');
    }

    public function Deal() {
        return $this->hasMany('App\Models\Transaksi\Deal','id','user_id');
    }

    public function Point() {
        return $this->hasMany('App\Models\Transaksi\Point','id','user_id');
    }
}
