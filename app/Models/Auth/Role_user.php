<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class Role_user extends Model
{

	protected $table = "role_user";
    protected $fillable = [
        'role_id',
        'user_id'
    ];

    public function role() {
    	return $this->belongsTo('App\Models\Auth\Role','role_id');
    }
}
