<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Model;

class Deal extends Model
{
    protected $table = "tran_deal";

    protected $fillable = [
        'id_pelanggan',
        'id_user',
        'no_agenda',
        'is_tmp',
        'is_approved',
        'is_rejected',
        'rejected_reason',
        'nilai_tmp',
        'tgl_bayar',
        'tgl_remaja',
    ];

    public function Pelanggan() {
        return $this->belongsTo('App\Models\Master\DataPelanggan','id_pelanggan','id');
    }
    
    public function User() {
        return $this->belongsTo('App\User','id_user','id');
    }
    
    public function Point() {
        return $this->belongsTo('App\Models\Transaksi\Point','id','id_deal');
    }

    public function Photo() {
        return $this->hasMany('App\Models\Transaksi\DataPelangganImage','id_pelanggan','id_pelanggan');
    }
}
