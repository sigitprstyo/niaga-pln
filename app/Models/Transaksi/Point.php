<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    protected $table = "tran_point";

    protected $fillable = [
        'tipe_transaksi',
        'kredit',
        'debit',
        'keterangan',
        'is_approve_point',
        'id_pelanggan',
        'id_deal',
        'id_reward',
        'id_user',
        'is_reject_point'
    ];
    
    public function User() {
        return $this->belongsTo('App\User','id_user','id');
    }

    public function Pelanggan() {
        return $this->belongsTo('App\Models\Master\DataPelanggan','id_pelanggan','id');
    }

    public function Deal() {
        return $this->belongsTo('App\Models\Transaksi\Deal','id_deal','id');
    }

    public function Reward() {
        return $this->belongsTo('App\Models\Master\Reward','id_reward','id');
    }
}
