<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Model;

class DataPelangganImage extends Model
{
    protected $table = "data_pelanggan_images";

    protected $fillable = [
        'id_pelanggan',
        'path_foto',
    ];
}
