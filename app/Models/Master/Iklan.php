<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Iklan extends Model
{
    protected $table = "iklan";

    protected $fillable = [
        'tipe',
        'path_img',
        'keterangan'
    ];
}
