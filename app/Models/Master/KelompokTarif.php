<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class KelompokTarif extends Model
{
    protected $table = "kelompok_tarifs";

    protected $fillable = [
        'code',
        'name',
        'point_probing_pb',
        'point_closing_pb',
        'point_probing_pd',
        'point_closing_pd'
    ];
}
