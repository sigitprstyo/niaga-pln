<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Daya extends Model
{
    protected $table = "daya";

    protected $fillable = [
        'daya'
    ];
}
