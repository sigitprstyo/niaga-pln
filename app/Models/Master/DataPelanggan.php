<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class DataPelanggan extends Model
{
    protected $table = "data_pelanggan";

    protected $fillable = [
        'no_ktp',
        'nama_pelanggan',
        'tarif',
        'daya',
        'data_pembangkit',
        'koordinat_x',
        'koordinat_y',
        'is_probing_individual',
        'path_foto',
        'path_ktp',
        'created_by',
        'tarif_lama',
        'daya_lama',
        'type',
        'merek',
        'kapasitas',
        'qty_unit',
        'kelompok',
        'alamat',
        'no_agenda',
        'is_approved',
        'is_rejected',
        'rejected_reason',
        'no_hp',
    ];

    public function User() {
        return $this->belongsTo('App\User','created_by','id');
    }

    public function Point() {
        return $this->belongsTo('App\Models\Transaksi\Point','id','id_pelanggan');
    }

    public function Deal() {
        return $this->hasOne('App\Models\Transaksi\Deal','id_pelanggan','id');
    }

    public function Photo() {
        return $this->hasMany('App\Models\Transaksi\DataPelangganImage','id_pelanggan','id');
    }
}
