<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Tarif extends Model
{
    protected $table = "tarif";

    protected $fillable = [
        'tipe',
        'harga'
    ];
}
