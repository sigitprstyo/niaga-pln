<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    protected $table = "rewards";

    protected $fillable = [
        'nama_reward',
        'deskripsi',
        'point',
        'path_image',
        'category'
    ];
}
