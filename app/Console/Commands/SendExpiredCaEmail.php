<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Models\Kredit\Kredit;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendExpiredMail;

class SendExpiredCaEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expired:credit-approval';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Expired Credit Approval to Users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //expired in the last 2 month
        $from = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-2 month" ) ); 
        $to = date('Y-m-d');
        $expired_list=Kredit::whereBetween('tempo_end', [$from, $to])->get();

        // Send Email
        $user= User::get();
        foreach ($user as $key) {
            Mail::to($key->email)->send(new SendExpiredMail($expired_list));
        }
    }
}
