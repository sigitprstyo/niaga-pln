<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Master\KelompokTarif;

class KelompokTarifController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     public function create()
    {
        return view('Master/KelompokTarif.create');
    }

     public function store(Request $request)
    {
        $this->validate(request(), [
           'code' => 'required|unique:kelompok_tarifs',
           'name' => 'required',
           'point_probing_pb' => 'required|numeric',
           'point_closing_pb' => 'required|numeric',
           'point_probing_pd' => 'required|numeric',
           'point_closing_pd' => 'required|numeric'
        ]);

        KelompokTarif::create([
          'code' => $request['code'],
          'name' => $request['name'],
          'point_probing_pb' => $request['point_probing_pb'],
          'point_closing_pb' => $request['point_closing_pb'],
          'point_probing_pd' => $request['point_probing_pd'],
          'point_closing_pd' => $request['point_closing_pd']
        ]);

        return redirect('Master/KelompokTarif')->with('success','Kelompok Tarif telah ditambahkan');
    }

     public function index()
    {
        $kelompoktarif = KelompokTarif::get();
        return view('Master/KelompokTarif.index', compact('kelompoktarif'));
        
    }

     public function edit($id)
    {
        $kelompoktarif = KelompokTarif::find($id);
        return view('Master/KelompokTarif.edit', compact('kelompoktarif'));
    }

     public function update(Request $request, $id)
    {
        $this->validate(request(), [
            'code' => 'required|unique:kelompok_tarifs,code,id,'.$id,
            'name' => 'required',
            'point_probing_pb' => 'required|numeric',
            'point_closing_pb' => 'required|numeric',
            'point_probing_pd' => 'required|numeric',
            'point_closing_pd' => 'required|numeric'
         ]);
        
        $kelompoktarif = KelompokTarif::find($id);
        $kelompoktarif->code = $request->get('code');
        $kelompoktarif->name = $request->get('name');
        $kelompoktarif->point_probing_pb = $request->get('point_probing_pb');
        $kelompoktarif->point_closing_pb = $request->get('point_closing_pb');
        $kelompoktarif->point_probing_pd = $request->get('point_probing_pd');
        $kelompoktarif->point_closing_pd = $request->get('point_closing_pd');

        $kelompoktarif->save();

       return redirect('Master/KelompokTarif')->with('success','Kelompok Tarif telah di ubah');
    }
  
    public function show($id)
    {
        // $customer = Customer::find($id);
        //  return view('Master/Customer.show', compact('customer'));
    }

    public function destroy($id)
    {
        $kelompoktarif = KelompokTarif::find($id);
        $kelompoktarif->delete();
        return redirect('Master/KelompokTarif')->with('success','Kelompok Tarif telah di hapus');
    }
}
