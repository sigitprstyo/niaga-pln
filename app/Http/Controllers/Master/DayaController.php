<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Master\Daya;

class DayaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     public function create()
    {
        return view('Master/Daya.create');
    }

     public function store(Request $request)
    {
        $this->validate(request(), [
           'daya' => 'required'
        ]);

        Daya::create([
          'daya' => $request['daya']
        ]);

        return redirect('Master/Daya')->with('success','Daya telah ditambahkan');
    }

     public function index()
    {
        $daya = Daya::get();
        return view('Master/Daya.index', compact('daya'));
        
    }

     public function edit($id)
    {
        $daya = Daya::find($id);
        return view('Master/Daya.edit', compact('daya'));
    }

     public function update(Request $request, $id)
    {
        $daya = Daya::find($id);
        $daya->daya = $request->get('daya');

        $daya->save();

       return redirect('Master/Daya')->with('success','Daya telah di ubah');
    }
  
    public function show($id)
    {
        // $customer = Customer::find($id);
        //  return view('Master/Customer.show', compact('customer'));
    }

    public function destroy($id)
    {
        $Daya = Daya::find($id);
        $Daya->delete();
        return redirect('Master/Daya')->with('success','Daya telah di hapus');
    }
}
