<?php

namespace App\Http\Controllers\Master;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Master\DataPelanggan as DataPelanggan;
use Session;
use Excel;
use File;

class DataPelangganController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     public function create()
    {
        return view('Master/Customer.create');
    }

     public function store(Request $request)
    {
        $this->validate(request(), [
           'customer_no' => 'required',
           'name' => 'required',
           'email' => 'required',
           'address' => 'required',
           'npwp' => 'required'
        ]);

        Customer::create([
          'customer_no' => $request['customer_no'],
          'name' => $request['name'],
          'email' => $request['email'],
          'address' => $request['address'],
          'npwp' => $request['npwp']
        ]);

        return redirect('Master/Customer')->with('success','Customer telah ditambahkan');
    }

     public function index()
    {
        $customer = Customer::get();
        return view('Master/Customer.index', compact('customer'));
        
    }

    public function filter(Request $request)
    {
         return view('Master/Customer.index');
    }

     public function edit($id)
    {
        $customer = Customer::find($id);
        return view('Master/Customer.edit', compact('customer'));
    }

     public function update(Request $request, $id)
    {
        $customer = Customer::find($id);
        $customer->customer_no = $request->get('customer_no');
        $customer->name = $request->get('name');
        $customer->email = $request->get('email');
        $customer->address = $request->get('address');
        $customer->npwp = $request->get('npwp');

        $customer->save();

       return redirect('Master/Customer')->with('success','Customer telah di ubah');
    }
  
    public function show($id)
    {
        // $customer = Customer::find($id);
        //  return view('Master/Customer.show', compact('customer'));
    }

     public function destroy($id)
    {
        $customer = Customer::find($id);
        $customer->delete();
        return redirect('Master/Customer')->with('success','Product telah di hapus');
    }

     public function export()
    {
        $customer = Customer::get();
        return $customer;
        // Excel::create(
        //     'Customer List', function ($excel) use ($customer) {
        //         $excel->sheet(
        //             'New sheet', function ($sheet) use ($customer) {
        //                 $sheet->loadView('Master.Customer.exportxl', compact('customer'));
        //             }
        //         );
        //     }
        // )->export('xls');


    }

    

public function UploadUpdate(Request $request){

        //validate the xls file
        $this->validate($request, array(
            'file'      => 'required'
        ));
 
        if($request->hasFile('file')){
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
 
                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();
                if(!empty($data) && $data->count()){
 
                    foreach ($data as $key => $value) {
                        $insert[] = [
                        'customer_no' => $value->customer_no,
                        'name' => $value->name,
                        'email' => $value->email,
                        'address' => $value->address,
                        'npwp' => $value->npwp,
                        ];
                    }

                    if(!empty($insert)){
                        $insertData = Customer::insert($insert);
                        if ($insertData) {
                            Session::flash('success', 'Your Data has successfully imported');
                        }else {                        
                            Session::flash('error', 'Error inserting the data..');
                            return back();
                        }
                    }
                }
 
                return back();
 
            }else {
                Session::flash('error', 'File is a '.$extension.' file.!! Please upload a valid xls/csv file..!!');
                return back();
            }
        }
}

}
