<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Master\Tarif;

class TarifController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     public function create()
    {
        return view('Master/Tarif.create');
    }

     public function store(Request $request)
    {
        $this->validate(request(), [
           'tipe' => 'required',
           'harga' => 'required'
        ]);

        Tarif::create([
          'tipe' => $request['tipe'],
          'harga' => $request['harga']
        ]);

        return redirect('Master/Tarif')->with('success','Tarif telah ditambahkan');
    }

     public function index()
    {
        $tarif = Tarif::get();
        return view('Master/Tarif.index', compact('tarif'));
        
    }

     public function edit($id)
    {
        $tarif = Tarif::find($id);
        return view('Master/Tarif.edit', compact('tarif'));
    }

     public function update(Request $request, $id)
    {
        $customer = Tarif::find($id);
        $customer->tipe = $request->get('tipe');
        $customer->harga = $request->get('harga');

        $customer->save();

       return redirect('Master/Tarif')->with('success','Tarif telah di ubah');
    }
  
    public function show($id)
    {
        // $customer = Customer::find($id);
        //  return view('Master/Customer.show', compact('customer'));
    }

    public function destroy($id)
    {
        $tarif = Tarif::find($id);
        $tarif->delete();
        return redirect('Master/Tarif')->with('success','Tarif telah di hapus');
    }
}
