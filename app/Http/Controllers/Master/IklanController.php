<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Master\Iklan;

class IklanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     public function create()
    {
        return view('Master/Iklan.create');
    }

     public function store(Request $request)
    {
        $this->validate(request(), [
           'tipe' => 'required'
        ]);

        $image = $request->file('path_image')->store('public/upload/images');
        $path = str_replace("public/","storage/", $image);

        Iklan::create([
          'tipe' => $request['tipe'],
          'path_img' => $path,
          'keterangan' => $request['keterangan']
        ]);

        return redirect('Master/Iklan')->with('success','Iklan telah ditambahkan');
    }

     public function index()
    {
        $iklan = Iklan::get();
        return view('Master/Iklan.index', compact('iklan'));
        
    }
     public function edit($id)
    {
        $iklan = Iklan::find($id);
        return view('Master/Iklan.edit', compact('iklan'));
    }

     public function update(Request $request, $id)
    {
        $this->validate(request(), [
            'tipe' => 'required'
        ]);
        
        $image = $request->file('path_image')->store('public/upload/images');
        $path = str_replace("public/","storage/", $image);

        $iklan = Iklan::find($id);
        $iklan->tipe = $request->get('tipe');
        $iklan->keterangan = $request->get('keterangan');

        if($request->has('path_image')){

            $image = $request->file('path_image')->store('public/upload/images');
            $path = str_replace("public/","storage/", $image);
            $iklan->path_img = $path;
        }

        $iklan->save();

       return redirect('Master/iklan')->with('success','iklan telah di ubah');
    }
  
    public function show($id)
    {
        // $customer = Customer::find($id);
        //  return view('Master/Customer.show', compact('customer'));
    }

    public function destroy($id)
    {
        $iklan = Iklan::find($id);
        $iklan->delete();
        return redirect('Master/Tipe')->with('success','Iklan telah di hapus');
    }
}
