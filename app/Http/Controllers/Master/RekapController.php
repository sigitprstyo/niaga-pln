<?php

namespace App\Http\Controllers\Master;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

use App\Models\Transaksi\Deal;
use App\Models\Master\DataPelanggan;

class RekapController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function rekap_pending(Request $request){

        $data_probing = DataPelanggan::where('is_rejected', 0)->where('is_approved', 0);
        $data_closing = Deal::where('is_rejected', 0)->where('is_approved', 0);
        
        if($request->has('from_tanggal') && $request->has('from_tanggal')){
            $data_probing->whereDate('created_at', '>=', $request->get('from_tanggal'))->whereDate('created_at', '<=', $request->get('to_tanggal'));
            $data_closing->whereDate('created_at', '>=', $request->get('from_tanggal'))->whereDate('created_at', '<=', $request->get('to_tanggal'));
        }

        $jml_probing = $data_probing->count();
        $jml_closing = $data_closing->count();

        return view('Master/Rekap.pending', compact('jml_probing', 'jml_closing'));
    }

    public function rekap_submitted(Request $request){

        $data_probing = DataPelanggan::where('is_approved', 1);
        $data_closing = Deal::where('is_approved', 1);
        
        if($request->has('from_tanggal') && $request->has('from_tanggal')){
            $data_probing->whereDate('created_at', '>=', $request->get('from_tanggal'))->whereDate('created_at', '<=', $request->get('to_tanggal'));
            $data_closing->whereDate('created_at', '>=', $request->get('from_tanggal'))->whereDate('created_at', '<=', $request->get('to_tanggal'));
        }

        $jml_probing = $data_probing->count();
        $jml_closing = $data_closing->count();

        return view('Master/Rekap.submitted', compact('jml_probing', 'jml_closing'));
    }

    public function rekap_completed(Request $request){

        return view('Master/Rekap.completed', compact('jml_probing', 'jml_closing'));
    }

    public function rekap_rejected(Request $request){

        $data_probing = DataPelanggan::where('is_rejected', 1);
        $data_closing = Deal::where('is_rejected', 1);
        
        $jml_probing = $data_probing->count();
        $jml_closing = $data_closing->count();

        return view('Master/Rekap.rejected', compact('jml_probing', 'jml_closing'));
    }

    public function pasang_baru(Request $request){

        $listdata = DB::table('users')->leftJoin('data_pelanggan as pel', 'users.id', '=', 'pel.created_by')
                        ->select('users.name', DB::raw('COUNT(pel.id) as jml_pelanggan'), DB::raw('SUM(daya) as total_daya'))->where('users.id','!=',1)
                        ->groupBy('users.name')->get();

        return view('Master/Rekap.pasangbaru', compact('listdata'));
    }

    public function perubahan_daya(Request $request){

        $listdata = DB::table('users')->leftJoin('data_pelanggan as pel', 'users.id', '=', 'pel.created_by')
                        ->select('users.name', DB::raw('COUNT(pel.id) as jml_pelanggan'), DB::raw('SUM(daya - daya_lama) as total_daya'))->where('users.id','!=',1)
                        ->groupBy('users.name')->get();

        return view('Master/Rekap.perubahandaya', compact('listdata'));
    }

    public function probing_closing(Request $request){

        $listdata = DB::table('users')->leftJoin('data_pelanggan as pel', 'users.id', '=', 'pel.created_by')
                        ->leftJoin('tran_deal as deal', 'pel.id', '=', 'deal.id_pelanggan')
                        ->select('users.name', DB::raw('COUNT(pel.id) as jml_probing'), DB::raw('COUNT(deal.id) as jml_closing'))->where('users.id','!=',1)
                        ->groupBy('users.name')->get();

        return view('Master/Rekap.probingclosing', compact('listdata'));
    }
}
