<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Master\Reward;

class RewardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     public function create()
    {
        return view('Master/Reward.create');
    }

     public function store(Request $request)
    {
        $this->validate(request(), [
           'nama_reward' => 'required',
           'point' => 'required',
           'image' => 'required'
        ]);

        $image = $request->file('image')->store('public/upload/images');
        $path = str_replace("public/","storage/", $image);
        
        $isactive = 0;

        if($request->has('aktif')){

            $isactive = $request['aktif'];
        }

        Reward::create([
          'nama_reward' => $request['nama_reward'],
          'deskripsi' => $request['deskripsi'],
          'point' => $request['point'],
          'path_image' => $path,
          'category' => $request['category'],
          'active' => $isactive
        ]);

        return redirect('Master/Reward')->with('success','Reward telah ditambahkan');
    }

     public function index()
    {
        $reward = Reward::get();
        return view('Master/Reward.index', compact('reward'));
        
    }

    public function filter(Request $request)
    {
         return view('Master/Reward.index');
    }

     public function edit($id)
    {
        $data = Reward::find($id);
        return view('Master/Reward.edit', compact('data'));
    }

     public function update(Request $request, $id)
    {
        

        $Reward = Reward::find($id);
        $Reward->nama_reward = $request->get('nama_reward');
        $Reward->deskripsi = $request->get('deskripsi');
        $Reward->point = $request->get('point');
        $Reward->category = $request->get('category');

        if($request->has('image')){

            $image = $request->file('image')->store('public/upload/images');
            $path = str_replace("public/","storage/", $image);
            $Reward->path_image = $path;
        }

        if($request->has('aktif')){

            $Reward->active = $request['aktif'];
        }

        $Reward->save();

       return redirect('Master/Reward')->with('success','Reward telah di ubah');
    }
  
    public function show($id)
    {
        // $Reward = Reward::find($id);
        //  return view('Master/Reward.show', compact('Reward'));
    }

    public function destroy($id)
    {
        $Reward = Reward::find($id);
        $Reward->delete();
        return redirect('Master/Reward')->with('success','Product telah di hapus');
    }
}
