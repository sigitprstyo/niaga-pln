<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use App\Models\Kredit\Kredit;
use App\Models\Transaksi\Deal;
use App\Models\Master\DataPelanggan;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listdata = DB::table('users')->leftJoin('data_pelanggan as pel', 'users.id', '=', 'pel.created_by')
                        ->leftJoin('tran_deal as deal', 'pel.id', '=', 'deal.id_pelanggan')
                        ->select('users.name', DB::raw('COUNT(pel.id) as jml_probing'), DB::raw('COUNT(deal.id) as jml_closing'))
                        ->groupBy('users.name')->get();

        $jml_probing_approved = DataPelanggan::where('is_approved', 1)->count();
        $jml_closing_approved = Deal::where('is_approved', 1)->count();

        $total_approved = $jml_probing_approved + $jml_closing_approved;

        $jml_probing_submitted = DataPelanggan::count();
        $jml_closing_submitted = Deal::count();

        $total_submitted = $jml_probing_submitted + $jml_closing_submitted;

        $jml_probing_reject = DataPelanggan::where('is_rejected', 1)->count();
        $jml_closing_reject = Deal::where('is_rejected', 1)->count();

        $total_reject = $jml_closing_reject + $jml_probing_reject;

        $jml_complete = Deal::whereHas('Pelanggan')->count();
        
        return view('home', compact('listdata','total_approved','total_submitted','total_reject','jml_complete'));
    }

    public function mailView($id)
    {
        $data=Kredit::find($id);
        return view('emails.name', compact('data'));
    }
}
