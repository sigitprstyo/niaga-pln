<?php

namespace App\Http\Controllers\Kredit;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaksi\Point;
use App\Models\Transaksi\Deal;
use App\Models\Master\DataPelanggan;
use DB;
use Carbon\Carbon;

class ApprovalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function pending_probing(){

        $list_point = DataPelanggan::where('is_rejected', '=', '0')->get();

        return view('Transaksi/Probing.index', compact('list_point'));
    }

    public function reject_probing(){

        $list_point = DataPelanggan::where('is_rejected', '=', '1')->get();

        return view('Transaksi/Probing.rejected', compact('list_point'));
    }

    public function form_probing_approval($id)
    {
        $data_probing = DataPelanggan::with(['Point','Photo','User'])->findOrFail($id);
        return view('Transaksi/Probing.process', compact('data_probing'));
    }

    public function process_probing(Request $request, $id)
    {
        try {
            $this->validate(request(), [
                'status' => 'required',
            ]);

            $data_probing = DataPelanggan::findOrFail($id);

            if($request['status'] == 1){
                $data_probing['is_approved'] = 1;
                $data_probing['is_rejected'] = 0;

                $update_point = Point::where('id_pelanggan', $id)->whereNull('id_deal')->update(['is_reject_point' => 0, 'is_approve_point' => 1]);
            }else{
                $data_probing['is_approved'] = 0;
                $data_probing['is_rejected'] = 1;
                $data_probing['rejected_reason'] = $request['catatan'];

                $update_point = Point::where('id_pelanggan', $id)->whereNull('id_deal')->update(['is_reject_point' => 1, 'is_approve_point' => 0, 'rejected_reason' => $request['catatan']]);
            }

            $data_probing->save();
            return response($data_probing, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => $th
            ], $th->status);
        }

    }

    public function pending_closing(){

        $list_point = Deal::with(['Pelanggan','User','Point'])->whereHas('Pelanggan', function ($query) {
            $query->where('is_approved', '=', '1');
        })->where('is_rejected', '=', '0')->get();

        return view('Transaksi/Closing.index', compact('list_point'));
    }

    public function reject_closing(){

        $list_point = Deal::with(['Pelanggan','User','Point'])->whereHas('Pelanggan', function ($query) {
            $query->where('is_approved', '=', '1');
        })->where('is_rejected', '=', '1')->get();

        return view('Transaksi/Closing.reject', compact('list_point'));
    }

    public function form_closing_approval($id)
    {
        $data_closing = Deal::with(['Pelanggan','User','Point','Photo'])->findOrFail($id);
        return view('Transaksi/Closing.process', compact('data_closing'));
    }

    public function process_closing(Request $request, $id)
    {
        try {
            $this->validate(request(), [
                'status' => 'required',
            ]);

            $data_closing = Deal::findOrFail($id);

            if($request['status'] == 1){
                $data_closing['is_approved'] = 1;
                $data_closing['is_rejected'] = 0;

                $update_point = Point::where('id_deal', $id)->update(['is_reject_point' => 0, 'is_approve_point' => 1]);
            }else{
                $data_closing['is_approved'] = 0;
                $data_closing['is_rejected'] = 1;
                $data_closing['rejected_reason'] = $request['catatan'];

                $update_point = Point::where('id_deal', $id)->update(['is_reject_point' => 1, 'is_approve_point' => 0, 'rejected_reason' => $request['catatan']]);
            }

            $data_closing->save();
            return response($data_closing, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => $th
            ], $th->status);
        }

    }

    public function verify_closing(Request $request, $id)
    {
        try {
            $this->validate(request(), [
                'tgl_bayar' => 'required',
                'tmp' => 'required',
                'tgl_remaja' => 'required',
            ]);

            DB::beginTransaction();

            $data_closing = Deal::findOrFail($id);

            $data_closing['nilai_tmp'] = $request['tmp'];
            $data_closing['tgl_bayar'] = $request['tgl_bayar'];
            $data_closing['tgl_remaja'] = $request['tgl_remaja'];

            $tgl_remaja = Carbon::createFromFormat('Y-m-d', $request['tgl_remaja']);
            $tgl_bayar = Carbon::createFromFormat('Y-m-d', $request['tgl_bayar']);
            $tgl_closing = Carbon::createFromFormat('Y-m-d H:i:s', $data_closing['created_at']);

            $update_point = Point::where('id_deal', $id)->first();

            if($tgl_closing >= $tgl_remaja){

                $data_closing['is_approved'] = 1;
                $data_closing['is_rejected'] = 0;

                $selisih = $tgl_remaja->diffInDays($tgl_bayar);

                $point = $update_point['kredit'];

                if ($selisih > $request['tmp']){
                    $point = $update_point['kredit'] * 0.75;
                }

                $update_point['is_reject_point'] = 0;
                $update_point['is_approve_point'] = 1;
                $update_point['init_point'] = $update_point['kredit'];
                $update_point['kredit'] = $point;

            }else{
                
                $data_closing['is_approved'] = 0;
                $data_closing['is_rejected'] = 1;
                $data_closing['rejected_reason'] = $request['catatan'];
                
                $update_point['is_reject_point'] = 1;
                $update_point['is_approve_point'] = 0;
                $update_point['init_point'] = 'Ditolak karena Tanggal Closing sebelum Tanggal Remaja';
                $update_point['init_point'] = $update_point['kredit'];
            }

            $update_point->save();

            $data_closing->save();
            
            DB::commit();


            // return response($data_closing, 200);

        } catch (\Throwable $th) {

            DB::rollBack();

            return response()->json([
                'status' => 'error',
                'message' => $th
            ], 422);
        }

    }

    public function pending_redeem(){

        $list_point = Point::with(['Pelanggan','User','Deal','Reward'])->whereNotNull ('id_reward')
                        ->where('is_reject_point', '=', '0')->get();

        return view('Transaksi/Redeem.index', compact('list_point'));
    }

    public function reject_redeem(){

        $list_point = Point::with(['Pelanggan','User','Deal','Reward'])->whereNotNull ('id_reward')
                        ->where('is_reject_point', '=', '1')->get();

        return view('Transaksi/Redeem.rejected', compact('list_point'));
    }

    public function form_redeem_approval($id)
    {
        $data_closing = Point::with(['Pelanggan','User','Deal','Reward'])->findOrFail($id);

        $kredit_point = Point::where('id_user', $data_closing['user']->id)->where('is_approve_point', 1)->where('is_reject_point', 0)->sum('kredit');
        $debit_point = Point::where('id_user', $data_closing['user']->id)->where('is_approve_point', 1)->where('is_reject_point', 0)->sum('debit');
        $total_point = $kredit_point - $debit_point;

        $data_closing['jml_point'] = $total_point;

        return view('Transaksi/Redeem.process', compact('data_closing'));
    }

    public function process_redeem(Request $request, $id)
    {
        try {
            $this->validate(request(), [
                'status' => 'required',
            ]);

            $data_reward = Point::findOrFail($id);

            if($request['status'] == 1){
                
                $data_reward['is_approve_point'] = 1;
                $data_reward['is_reject_point'] = 0;

            }else{
                
                $data_reward['is_approve_point'] = 0;
                $data_reward['is_reject_point'] = 1;
                $data_reward['rejected_reason'] = $request['catatan'];
            }

            $data_reward->save();
            return response($data_reward, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => $th
            ], 422);
        }

    }

    public function form_auto_approval()
    {
        return view('Transaksi/AutoApproval.form');
    }

    public function process_auto(Request $request)
    {
        try {
            $this->validate(request(), [
                'datajson' => 'required',
            ]);
            
            $arr_date = array();

            $data = json_decode($request['datajson']);

            return var_dump($data);

            return response(null, 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => $th
            ], 422);
        }

    }
}
