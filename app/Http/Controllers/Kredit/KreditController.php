<?php

namespace App\Http\Controllers\Kredit;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Kredit\Kredit;
use App\Models\Master\Customer;
use App\Models\Master\Product;
use App\Models\Master\Ttd;
use App\Models\Transaction\CaNota;
use App\User;
use App\Models\Transaction\CaProduct;
use App\Models\Transaction\CaActionNote;
use App\Models\Transaction\NumGenerator;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use PDF;
use DB;

class KreditController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

     public function create() 
    {
      $bank = array('Amar Bank Indonesia',
                    'Bangkok Bank',
                    'Bank Agris',
                    'Bank Anda',
                    'Bank Andara',
                    'Bank ANZ Indonesia',
                    'Bank Artha Graha Indonesia',
                    'Bank Artos Indonesia',
                    'Bank Bengkulu',
                    'Bank Bisnis Internasional',
                    'Bank BJB',
                    'Bank BJB Syariah',
                    'Bank BNI Syariah',
                    'Bank BNP Paribas Indonesia',
                    'Bank BPD Aceh',
                    'Bank BPD Aceh Syariah',
                    'Bank BPD Bali',
                    'Bank BPD DIY',
                    'Bank BPD Sulteng',
                    'Bank BRI Agroniaga',
                    'Bank BRI Syariah',
                    'Bank Bumi Arta',
                    'Bank Capital Indonesia',
                    'Bank Central Asia (BCA)',
                    'Bank Chinatrust Indonesia',
                    'Bank CIMB Niaga',
                    'Bank Danamon Indonesia',
                    'Bank Danamon Syariah',
                    'Bank DBS Indonesia',
                    'Bank Dinar Indonesia',
                    'Bank DKI',
                    'Bank DKI Syariah',
                    'Bank Ekonomi Raharja',
                    'Bank Fana Internasional',
                    'Bank Ganesha',
                    'Bank Harda Internasional',
                    'Bank ICBC Indonesia',
                    'Bank Ina Perdana',
                    'Bank Index Selindo',
                    'Bank Indonesia (BI)',
                    'Bank J Trust Indonesia',
                    'Bank Jambi',
                    'Bank Jasa Jakarta',
                    'Bank Jateng',
                    'Bank Jatim',
                    'Bank Kalbar',
                    'Bank Kalbar Syariah',
                    'Bank Kalteng',
                    'Bank Kaltim',
                    'Bank Kaltim Syariah',
                    'Bank KEB Hana',
                    'Bank Kesejahteraan Ekonomi',
                    'Bank Mandiri',
                    'Bank Maspion',
                    'Bank Mayapada',
                    'Bank Maybank Indonesia',
                    'Bank Maybank Syariah Indonesia',
                    'Bank Mayora',
                    'Bank Mega',
                    'Bank Mega Syariah',
                    'Bank Mestika Dharma',
                    'Bank Mitraniaga',
                    'Bank Mizuho Indonesia',
                    'Bank MNC Internasional',
                    'Bank Muamalat Indonesia',
                    'Bank Multi Arta Sentosa',
                    'Bank Nagari',
                    'Bank Nagari Syariah',
                    'Bank Nationalnobu',
                    'Bank Negara Indonesia (BNI)',
                    'Bank NTB',
                    'Bank Nusantara Parahyangan',
                    'Bank OCBC NISP',
                    'Bank Of America',
                    'Bank Of China',
                    'Bank Of India Indonesia',
                    'Bank Papua',
                    'Bank Permata',
                    'Bank Permata Syariah',
                    'Bank Pundi Indonesia',
                    'Bank QNB Indonesia',
                    'Bank Rabobank International Indonesia',
                    'Bank Rakyat Indonesia (BRI)',
                    'Bank Pesona Perdania',
                    'Bank Riau Kepri',
                    'Bank Riau Kepri Syariah',
                    'Bank Royal Indonesia',
                    'Bank Sahabat Sampoerna',
                    'Bank SBI Indonesia',
                    'Bank Shinhan Indonesia',
                    'Bank Sinarmas',
                    'Bank Sulsel',
                    'Bank Sultra',
                    'Bank Sulut',
                    'Bank Sumsel Babel',
                    'Bank Sumsel Babel Syariah',
                    'Bank Sumitomo Mitsui Indonesia',
                    'Bank Sumut',
                    'Bank Sumut Syariah',
                    'Bank Syariah Bukopin',
                    'Bank Syariah Mandiri',
                    'Bank Tabungan Negara (BTN)',
                    'Bank Tabungan Pensiunan Nasional',
                    'Bank UOB Indonesia',
                    'Bank Victoria Internasional',
                    'Bank Victoria Syariah',
                    'Bank Windu Kentjana International',
                    'Bank Woori Saudara',
                    'Bank Yudha Bhakti',
                    'BCA Syariah',
                    'BII Syariah',
                    'BTPN Syariah',
                    'CIMB Niaga Syariah',
                    'Citibank',
                    'Deutsche Bank',
                    'HSBC',
                    'JPMorgan Chase',
                    'OCBC NISP Syariah',
                    'Panin Bank',
                    'Panin Bank Syariah',
                    'Prima Master Bank',
                    'Standart Chartered',
                    'The Bank of Tokyo-Mitsubishi UFJ',
                    'Lainnya');

      $jaminan = array('Tidak Ada',
                      'Letter Of Credit',
                      'Surat Kredit Berdokumen Dalam Negeri',
                      'Bank Garansi',
                      'Lainnya');

      $syarat_penyerahan = array('Franco',
                                'Loco',
                                'Lainnya');

      $jatuh_tempo = array('0001 Payable immediately Due net',
                          '0002 within 14 days 2 % cash discount',
                          '0002 within 30 days Due net',
                          '0003 within 14 days 3 % cash discount',
                          '0003 within 20 days 2 % cash discount',
                          '0003 within 30 days Due net',
                          '0004 Payable immediately Due net',
                          '0004 Baseline date on End of the month',
                          '0005 Payable immediately Due net',
                          '0005 Baseline date on 10 of next month',
                          '0006 Before End of the month 4 % cash discount',
                          '0006 Before 15 of the next month ; 2 % cash discount',
                          '0006 Before 15 in 2 months Due net',
                          '0007 For Invoicing up to 15 of Month',
                          '0007 Before 15 of the next month ; 2 % cash discount',
                          '0007 Before End of the next month ; Due net',
                          '0007 For Invoicing up to End of Month',
                          '0007 Before End of the next month ; 2 % cash discoun',
                          '0007 Before 15 in 2 months Due net',
                          '0008 For Invoicing up to 15 of Month',
                          '0008 within 14 days 2 % cash discount',
                          '0008 within 30 days 1,5 % cash discount',
                          '0008 within 45 days Due net',
                          '0008 Baseline date on 30 of the month',
                          '0008 For Invoicing up to End of Month',
                          '0008 within 14 days 2 % cash discount',
                          '0008 within 30 days 1,5 % cash discount',
                          '0008 within 45 days Due net',
                          '0008 Baseline date on 15 of next month',
                          '0009 Payable in 3 partial amounts',
                          '0009 1 installment: 30,000 % with payment term 0001',
                          '0009 2 installment: 40,000 % with payment term 0001',
                          '0009 3 installment: 30,000 % with payment term 0001',
                          'C000 Within 30 days Due net',
                          'R000 Within 1 day Due net',
                          'R001 Within 7 days Due net',
                          'R002 Within 10 days Due net',
                          'R003 Within 15 days Due net',
                          'R004 Within 20 days Due net',
                          'R005 Within 30 days Due net',
                          'R006 Before 15 of the next month ; Due net',
                          'R007 Before End in 2 months Due net',
                          'R008 Before 15 in 2 months Due net',
                          'R009 Before End of the next month ; Due net',
                          'R010 Within 90 days Due net',
                          'R011 Within 25 days Due net',
                          'R012 Within 29 days Due net',
                          'Z000 Within 6 days due net',
                          'Z001 Within 14 days due net',
                          'Z002 Within 30 days Due net',
                          'Z003 Within 8 days due net',
                          'Z004 Within 10 days due net',
                          'Z007 Within 120 days due net',
                          'Z008 Within 4 days due net',
                          'Z009 Within 40 days due net',
                          'Z010 Within 45 days due net',
                          'Z011 Within 3 days due net',
                          'Z012 Within 35 days due net',
                          'Z013 Within 5 days due net',
                          'Z014 Within 9 days due net',
                          'Z015 Within 21 days due net',
                          'Z016 Within 26 days due net',
                          'Z017 Within 23 days due net',
                          'Z018 Within 27 days due net',
                          'Z029 Within 30 days due net after BL date - count as 1',
                          'Z030 Within 180 days due net',
                          'ZC01 Within day auto collections',
                          'ZC03 Within 3 days auto collection',
                          'ZC07 Within 7 days auto collection',
                          'ZC10 Within 10 days auto collection',
                          'ZC14 Within 14 days auto collection',
                          'ZC21 Within 21 days auto collection',
                          'ZC30 Within 30 days auto collection',
                          'ZP07 Within 7 days Due net',
                          'ZP15 Within 15 days Due net',
                          'ZP30 Within 30 days Due net',
                          'ZP45 Within 45 days Due net',
                          'ZP60 Within 60 days Due net',
                          'ZP61 within 60 days due net',
                          'ZR01 Payment Term for EDC',
                          'ZR02 Payment Term for Deposit',
                          'ZR03 Credit within 4 days Due net',
                          'Lainnya');

      $periode_penagihan = array('Harian',
                                'Per 7 Harian',
                                'Per 14 Harian',
                                'Per 15 Harian',
                                'Bulanan',
                                );

      $customer=Customer::get();
      $product=Product::get();
      return view('Kredit.create', compact('customer', 'product', 'bank', 'jaminan', 'syarat_penyerahan', 'jatuh_tempo', 'periode_penagihan'));
    }

     public function store(Request $request)
    {
        $input = $this->validate(request(), [
           'no_surat' => 'required|unique:credit_approval',
           'customer' => 'required',
           'product' => 'required',
           'volume' => 'required|numeric',
           'satuan' => 'required',
           'periode_volume' => 'required',
           'nilai_transaksi' => 'required',
           'credit_limit' => 'required',
           'pembayaran' => 'required',
           'jaminan' => 'required',
           'syarat_penyerahan' => 'required',
           'tempo_start' => 'required',
           'tempo_end' => 'required',
           'lama_tempo' => 'required',
           'memo_pengantar' => 'required|mimes:pdf',
           'doc_lka' => 'required_without_all:doc_cas,doc_bg,doc_pml|mimes:pdf',
           'doc_cas' => 'required_without_all:doc_lka,doc_bg,doc_pml|mimes:pdf',
           'doc_bg' => 'required_without_all:doc_cas,doc_lka,doc_pml|mimes:pdf',
           'doc_pml' => 'required_without_all:doc_cas,doc_bg,doc_lka|mimes:pdf'
        ]);

        $memo_pengantar = $request->file('memo_pengantar');
        $memo_pengantar_extension = $memo_pengantar->getClientOriginalExtension();
        $memo_pengantar_fileName = str_random(5)."-".date('his')."-".str_random(3).".".$memo_pengantar_extension;
        $memo_pengantar_folderpath  = 'storage/upload/memo_pengantar'.'/';
        $memo_pengantar->move($memo_pengantar_folderpath , $memo_pengantar_fileName);

        if ($request->hasFile('doc_lka')) {
        $doc_lka = $request->file('doc_lka');
        $doc_lka_extension = $doc_lka->getClientOriginalExtension();
        $doc_lka_fileName = str_random(5)."-".date('his')."-".str_random(3).".".$doc_lka_extension;
        $doc_lka_folderpath  = 'storage/upload/doc_lka'.'/';
        $doc_lka->move($doc_lka_folderpath , $doc_lka_fileName);
        }

        if ($request->hasFile('doc_cas')) {
        $doc_cas = $request->file('doc_cas');
        $doc_cas_extension = $doc_cas->getClientOriginalExtension();
        $doc_cas_fileName = str_random(5)."-".date('his')."-".str_random(3).".".$doc_cas_extension;
        $doc_cas_folderpath  = 'storage/upload/doc_cas'.'/';
        $doc_cas->move($doc_cas_folderpath , $doc_cas_fileName);
        }

        if ($request->hasFile('doc_bg')) {
        $doc_bg = $request->file('doc_bg');
        $doc_bg_extension = $doc_bg->getClientOriginalExtension();
        $doc_bg_fileName = str_random(5)."-".date('his')."-".str_random(3).".".$doc_bg_extension;
        $doc_bg_folderpath  = 'storage/upload/doc_bg'.'/';
        $doc_bg->move($doc_bg_folderpath , $doc_bg_fileName);
        }

        if ($request->hasFile('doc_pml')) {
        $doc_pml = $request->file('doc_pml');
        $doc_pml_extension = $doc_pml->getClientOriginalExtension();
        $doc_pml_fileName = str_random(5)."-".date('his')."-".str_random(3).".".$doc_pml_extension;
        $doc_pml_folderpath  = 'storage/upload/doc_pml'.'/';
        $doc_pml->move($doc_pml_folderpath , $doc_pml_fileName);
        }

        // Generate Nomor Tiket reset per hari
        $ticket_no=NumGenerator::find(1);
        if (date("Ymd", strtotime($ticket_no->updated_at))==date("Ymd")) {
            $serial=$ticket_no->number+1;
            $serial=sprintf("%02d",$serial);
        }else{
            $serial=sprintf("%02d", 1);
        }
        $ticket_no->number = $serial;
        $ticket_no->save();

        $serial_number='CA'.date('ymd').$serial;

        if($request['pembayaran']=='Lainnya'){
            $pembayaran = $request['pembayaran_lain'];
        }else{
          $pembayaran = $request['pembayaran'];
        }

        if($request['jaminan']=='Lainnya'){
            $jaminan = $request['jaminan_lain'];
        }else{
          $jaminan = $request['jaminan'];
        }
        if($request['syarat_penyerahan']=='Lainnya'){
            $syarat_penyerahan = $request['syarat_penyerahan_lain'];
        }else{
          $syarat_penyerahan = $request['syarat_penyerahan'];
        }
        if($request['lama_tempo']=='Lainnya'){
            $lama_tempo = $request['lama_tempo_lain'];
        }else{
          $lama_tempo = $request['lama_tempo'];
        }

        $Kredit=Kredit::create([
          'no_tiket' => $serial_number,
          'no_surat' => $request['no_surat'],
          'customer_id' => $request['customer'],
          'volume' => str_replace('.','', $request['volume']),
          'satuan' => $request['satuan'],
          'periode_volume' => $request['periode_volume'],
          'nilai_transaksi' => str_replace('.','', $request['nilai_transaksi']),
          'credit_limit' => str_replace('.','', $request['credit_limit']),
          'pembayaran' => $pembayaran,
          'jaminan' => $jaminan,
          'periode_penyerahan' => $request['periode_penyerahan'],
          'syarat_penyerahan' => $syarat_penyerahan,
          'flag_denda' => $request['flag_denda'],
          'tempo_start' => $request['tempo_start'],
          'tempo_end' => $request['tempo_end'],
          'lama_tempo' => $lama_tempo,
          'memo_pengantar' => $memo_pengantar_folderpath.$memo_pengantar_fileName,
          'doc_lka' => $doc_lka_folderpath.$doc_lka_fileName,
          'doc_cas' => $doc_cas_folderpath.$doc_cas_fileName,
          'doc_bg' => $doc_bg_folderpath.$doc_bg_fileName,
          'doc_pml' => $doc_pml_folderpath.$doc_pml_fileName,
          'created_by' => Auth::user()->id,
          // 'status' => '1'
        ]);

        foreach ($request['product'] as $key => $value) {
            $ca_product=CaProduct::create([
              'credit_approval_id' => $Kredit->id,
              'product_id' => $value
            ]);
        }

        // $this->sendEmail($Kredit->customer_id, 3, $Kredit->id);

        return redirect('Kredit/'.$Kredit->id.'')->with('success','Kredit Berhasil Disimpan dengan nomor: '.$serial_number.'');

    }

     public function index(Request $request)
    { 
      if (Auth::user()->isRole('user')) {
          $kredit=Kredit::where('status', '>=', 0)->orderBy('created_at', 'desc')->get();
      }elseif (Auth::user()->isRole('ar')) {
          $kredit=Kredit::where('status', '>=', 1)->orderBy('created_at', 'desc')->get();
      }elseif (Auth::user()->isRole('cashbank')) {
          $kredit=Kredit::where('status', '>=', 2)->orderBy('created_at', 'desc')->get();
      }elseif (Auth::user()->isRole('fbs')) {
          $kredit=Kredit::where('status', '>=', 3)->orderBy('created_at', 'desc')->get();
      }elseif (Auth::user()->isRole('manajemenresiko')) {
          $kredit=Kredit::where('status', '>=', 4)->orderBy('created_at', 'desc')->get();
      }elseif (Auth::user()->isRole('komitekredit')) {
          $kredit=Kredit::where('status', '>=', 5)->orderBy('created_at', 'desc')->get();
      }elseif (Auth::user()->isRole('administrator')) {
          $kredit=Kredit::get();
      }

      return view('Kredit.index', compact('kredit'));   
    }

    public function expired(Request $request)
    { 
      //expired in the last 2 month
      $from = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-2 month" ) ); 
      $to = date('Y-m-d');

      if (Auth::user()->isRole('user')) {
          $kredit=Kredit::where('status', '>=', 0)->whereBetween('tempo_end', [$from, $to])->orderBy('created_at', 'desc')->get();
      }elseif (Auth::user()->isRole('ar')) {
          $kredit=Kredit::where('status', '>=', 1)->whereBetween('tempo_end', [$from, $to])->orderBy('created_at', 'desc')->get();
      }elseif (Auth::user()->isRole('cashbank')) {
          $kredit=Kredit::where('status', '>=', 2)->whereBetween('tempo_end', [$from, $to])->orderBy('created_at', 'desc')->get();
      }elseif (Auth::user()->isRole('fbs')) {
          $kredit=Kredit::where('status', '>=', 3)->whereBetween('tempo_end', [$from, $to])->orderBy('created_at', 'desc')->get();
      }elseif (Auth::user()->isRole('manajemenresiko')) {
          $kredit=Kredit::where('status', '>=', 4)->whereBetween('tempo_end', [$from, $to])->orderBy('created_at', 'desc')->get();
      }elseif (Auth::user()->isRole('komitekredit')) {
          $kredit=Kredit::where('status', '>=', 5)->whereBetween('tempo_end', [$from, $to])->orderBy('created_at', 'desc')->get();
      }elseif (Auth::user()->isRole('administrator')) {
          $kredit=Kredit::get();
      }

      return view('Kredit.expired', compact('kredit'));   
    }

    public function filter(Request $request)
    {
         if (Auth::user()->isRole('user')) {
          $kredit=Kredit::where([['status', '>=', 0],['status','like',$request['status']]])->whereBetween('created_at', [$request->get('from_tanggal')." 00:00:00", $request->get('to_tanggal')." 23:59:59"])->orderBy('created_at', 'desc')->get();
        }elseif (Auth::user()->isRole('ar')) {
            $kredit=Kredit::where([['status', '>=', 1],['status','like',$request['status']]])->whereBetween('created_at', [$request->get('from_tanggal')." 00:00:00", $request->get('to_tanggal')." 23:59:59"])->orderBy('created_at', 'desc')->get();
        }elseif (Auth::user()->isRole('cashbank')) {
            $kredit=Kredit::where([['status', '>=', 2],['status','like',$request['status']]])->whereBetween('created_at', [$request->get('from_tanggal')." 00:00:00", $request->get('to_tanggal')." 23:59:59"])->orderBy('created_at', 'desc')->get();
        }elseif (Auth::user()->isRole('fbs')) {
            $kredit=Kredit::where([['status', '>=', 3],['status','like',$request['status']]])->whereBetween('created_at', [$request->get('from_tanggal')." 00:00:00", $request->get('to_tanggal')." 23:59:59"])->orderBy('created_at', 'desc')->get();
        }elseif (Auth::user()->isRole('manajemenresiko')) {
            $kredit=Kredit::where([['status', '>=', 4],['status','like',$request['status']]])->whereBetween('created_at', [$request->get('from_tanggal')." 00:00:00", $request->get('to_tanggal')." 23:59:59"])->orderBy('created_at', 'desc')->get();
        }elseif (Auth::user()->isRole('komitekredit')) {
            $kredit=Kredit::where([['status', '>=', 5],['status','like',$request['status']]])->whereBetween('created_at', [$request->get('from_tanggal')." 00:00:00", $request->get('to_tanggal')." 23:59:59"])->orderBy('created_at', 'desc')->get();
        }elseif (Auth::user()->isRole('administrator')) {
            $kredit=Kredit::get();
      }
        return view('Kredit.index', compact('kredit'));
    }

     public function edit($id)
    {


      $bank = array('Amar Bank Indonesia',
                    'Bangkok Bank',
                    'Bank Agris',
                    'Bank Anda',
                    'Bank Andara',
                    'Bank ANZ Indonesia',
                    'Bank Artha Graha Indonesia',
                    'Bank Artos Indonesia',
                    'Bank Bengkulu',
                    'Bank Bisnis Internasional',
                    'Bank BJB',
                    'Bank BJB Syariah',
                    'Bank BNI Syariah',
                    'Bank BNP Paribas Indonesia',
                    'Bank BPD Aceh',
                    'Bank BPD Aceh Syariah',
                    'Bank BPD Bali',
                    'Bank BPD DIY',
                    'Bank BPD Sulteng',
                    'Bank BRI Agroniaga',
                    'Bank BRI Syariah',
                    'Bank Bumi Arta',
                    'Bank Capital Indonesia',
                    'Bank Central Asia (BCA)',
                    'Bank Chinatrust Indonesia',
                    'Bank CIMB Niaga',
                    'Bank Danamon Indonesia',
                    'Bank Danamon Syariah',
                    'Bank DBS Indonesia',
                    'Bank Dinar Indonesia',
                    'Bank DKI',
                    'Bank DKI Syariah',
                    'Bank Ekonomi Raharja',
                    'Bank Fana Internasional',
                    'Bank Ganesha',
                    'Bank Harda Internasional',
                    'Bank ICBC Indonesia',
                    'Bank Ina Perdana',
                    'Bank Index Selindo',
                    'Bank Indonesia (BI)',
                    'Bank J Trust Indonesia',
                    'Bank Jambi',
                    'Bank Jasa Jakarta',
                    'Bank Jateng',
                    'Bank Jatim',
                    'Bank Kalbar',
                    'Bank Kalbar Syariah',
                    'Bank Kalteng',
                    'Bank Kaltim',
                    'Bank Kaltim Syariah',
                    'Bank KEB Hana',
                    'Bank Kesejahteraan Ekonomi',
                    'Bank Mandiri',
                    'Bank Maspion',
                    'Bank Mayapada',
                    'Bank Maybank Indonesia',
                    'Bank Maybank Syariah Indonesia',
                    'Bank Mayora',
                    'Bank Mega',
                    'Bank Mega Syariah',
                    'Bank Mestika Dharma',
                    'Bank Mitraniaga',
                    'Bank Mizuho Indonesia',
                    'Bank MNC Internasional',
                    'Bank Muamalat Indonesia',
                    'Bank Multi Arta Sentosa',
                    'Bank Nagari',
                    'Bank Nagari Syariah',
                    'Bank Nationalnobu',
                    'Bank Negara Indonesia (BNI)',
                    'Bank NTB',
                    'Bank Nusantara Parahyangan',
                    'Bank OCBC NISP',
                    'Bank Of America',
                    'Bank Of China',
                    'Bank Of India Indonesia',
                    'Bank Papua',
                    'Bank Permata',
                    'Bank Permata Syariah',
                    'Bank Pundi Indonesia',
                    'Bank QNB Indonesia',
                    'Bank Rabobank International Indonesia',
                    'Bank Rakyat Indonesia (BRI)',
                    'Bank Pesona Perdania',
                    'Bank Riau Kepri',
                    'Bank Riau Kepri Syariah',
                    'Bank Royal Indonesia',
                    'Bank Sahabat Sampoerna',
                    'Bank SBI Indonesia',
                    'Bank Shinhan Indonesia',
                    'Bank Sinarmas',
                    'Bank Sulsel',
                    'Bank Sultra',
                    'Bank Sulut',
                    'Bank Sumsel Babel',
                    'Bank Sumsel Babel Syariah',
                    'Bank Sumitomo Mitsui Indonesia',
                    'Bank Sumut',
                    'Bank Sumut Syariah',
                    'Bank Syariah Bukopin',
                    'Bank Syariah Mandiri',
                    'Bank Tabungan Negara (BTN)',
                    'Bank Tabungan Pensiunan Nasional',
                    'Bank UOB Indonesia',
                    'Bank Victoria Internasional',
                    'Bank Victoria Syariah',
                    'Bank Windu Kentjana International',
                    'Bank Woori Saudara',
                    'Bank Yudha Bhakti',
                    'BCA Syariah',
                    'BII Syariah',
                    'BTPN Syariah',
                    'CIMB Niaga Syariah',
                    'Citibank',
                    'Deutsche Bank',
                    'HSBC',
                    'JPMorgan Chase',
                    'OCBC NISP Syariah',
                    'Panin Bank',
                    'Panin Bank Syariah',
                    'Prima Master Bank',
                    'Standart Chartered',
                    'The Bank of Tokyo-Mitsubishi UFJ',
                    'Lainnya');

      $jaminan = array('Tidak Ada',
                      'Letter Of Credit',
                      'Surat Kredit Berdokumen Dalam Negeri',
                      'Bank Garansi',
                      'Lainnya');

      $syarat_penyerahan = array('Franco',
                                'Loco',
                                'Lainnya');
      $jatuh_tempo = array('0001 Payable immediately Due net',
                          '0002 within 14 days 2 % cash discount',
                          '0002 within 30 days Due net',
                          '0003 within 14 days 3 % cash discount',
                          '0003 within 20 days 2 % cash discount',
                          '0003 within 30 days Due net',
                          '0004 Payable immediately Due net',
                          '0004 Baseline date on End of the month',
                          '0005 Payable immediately Due net',
                          '0005 Baseline date on 10 of next month',
                          '0006 Before End of the month 4 % cash discount',
                          '0006 Before 15 of the next month ; 2 % cash discount',
                          '0006 Before 15 in 2 months Due net',
                          '0007 For Invoicing up to 15 of Month',
                          '0007 Before 15 of the next month ; 2 % cash discount',
                          '0007 Before End of the next month ; Due net',
                          '0007 For Invoicing up to End of Month',
                          '0007 Before End of the next month ; 2 % cash discoun',
                          '0007 Before 15 in 2 months Due net',
                          '0008 For Invoicing up to 15 of Month',
                          '0008 within 14 days 2 % cash discount',
                          '0008 within 30 days 1,5 % cash discount',
                          '0008 within 45 days Due net',
                          '0008 Baseline date on 30 of the month',
                          '0008 For Invoicing up to End of Month',
                          '0008 within 14 days 2 % cash discount',
                          '0008 within 30 days 1,5 % cash discount',
                          '0008 within 45 days Due net',
                          '0008 Baseline date on 15 of next month',
                          '0009 Payable in 3 partial amounts',
                          '0009 1 installment: 30,000 % with payment term 0001',
                          '0009 2 installment: 40,000 % with payment term 0001',
                          '0009 3 installment: 30,000 % with payment term 0001',
                          'C000 Within 30 days Due net',
                          'R000 Within 1 day Due net',
                          'R001 Within 7 days Due net',
                          'R002 Within 10 days Due net',
                          'R003 Within 15 days Due net',
                          'R004 Within 20 days Due net',
                          'R005 Within 30 days Due net',
                          'R006 Before 15 of the next month ; Due net',
                          'R007 Before End in 2 months Due net',
                          'R008 Before 15 in 2 months Due net',
                          'R009 Before End of the next month ; Due net',
                          'R010 Within 90 days Due net',
                          'R011 Within 25 days Due net',
                          'R012 Within 29 days Due net',
                          'Z000 Within 6 days due net',
                          'Z001 Within 14 days due net',
                          'Z002 Within 30 days Due net',
                          'Z003 Within 8 days due net',
                          'Z004 Within 10 days due net',
                          'Z007 Within 120 days due net',
                          'Z008 Within 4 days due net',
                          'Z009 Within 40 days due net',
                          'Z010 Within 45 days due net',
                          'Z011 Within 3 days due net',
                          'Z012 Within 35 days due net',
                          'Z013 Within 5 days due net',
                          'Z014 Within 9 days due net',
                          'Z015 Within 21 days due net',
                          'Z016 Within 26 days due net',
                          'Z017 Within 23 days due net',
                          'Z018 Within 27 days due net',
                          'Z029 Within 30 days due net after BL date - count as 1',
                          'Z030 Within 180 days due net',
                          'ZC01 Within day auto collections',
                          'ZC03 Within 3 days auto collection',
                          'ZC07 Within 7 days auto collection',
                          'ZC10 Within 10 days auto collection',
                          'ZC14 Within 14 days auto collection',
                          'ZC21 Within 21 days auto collection',
                          'ZC30 Within 30 days auto collection',
                          'ZP07 Within 7 days Due net',
                          'ZP15 Within 15 days Due net',
                          'ZP30 Within 30 days Due net',
                          'ZP45 Within 45 days Due net',
                          'ZP60 Within 60 days Due net',
                          'ZP61 within 60 days due net',
                          'ZR01 Payment Term for EDC',
                          'ZR02 Payment Term for Deposit',
                          'ZR03 Credit within 4 days Due net',
                          'Lainnya');

      $periode_penagihan = array('Harian',
                                'Per 7 Harian',
                                'Per 14 Harian',
                                'Per 15 Harian',
                                'Bulanan',
                                );


        $kredit = Kredit::find($id);

        if(!in_array($kredit->pembayaran, $bank)){
          $kredit->pembayaran_lain=$kredit->pembayaran;
          $kredit->pembayaran='Lainnya';
        }
        if(!in_array($kredit->jaminan, $jaminan)){
          $kredit->jaminan_lain=$kredit->jaminan;
          $kredit->jaminan='Lainnya';
        }
        if(!in_array($kredit->syarat_penyerahan, $syarat_penyerahan)){
          $kredit->syarat_penyerahan_lain=$kredit->syarat_penyerahan;
          $kredit->syarat_penyerahan='Lainnya';
        }
        if(!in_array($kredit->lama_tempo, $jatuh_tempo)){
          $kredit->lama_tempo_lain=$kredit->lama_tempo;
          $kredit->lama_tempo='Lainnya';
        }

        $customer=Customer::get();
        $product=Product::get();
        return view('Kredit.edit', compact('kredit','customer','product', 'bank', 'jaminan', 'syarat_penyerahan', 'jatuh_tempo', 'periode_penagihan'));
    }

     public function update(Request $request, $id)
    {
        $input = $this->validate(request(), [
           'no_surat' => 'required',
           'customer' => 'required',
           'product' => 'required',
           'volume' => 'required|numeric',
           'satuan' => 'required',
           'periode_volume' => 'required',
           'nilai_transaksi' => 'required',
           'credit_limit' => 'required',
           'pembayaran' => 'required',
           'jaminan' => 'required',
           'periode_penyerahan' => 'required',
           'syarat_penyerahan' => 'required',
           'tempo_start' => 'required',
           'tempo_end' => 'required',
           'lama_tempo' => 'required',
           // 'memo_pengantar' => 'required|mimes:pdf',
           // 'doc_lka' => 'required|mimes:pdf'
        ]);

        if($request->file('memo_pengantar')!=''){ 
          $input = $this->validate(request(), [
             'memo_pengantar' => 'required|mimes:pdf',
          ]);
          $memo_pengantar = $request->file('memo_pengantar');
          $memo_pengantar_extension = $memo_pengantar->getClientOriginalExtension();
          $memo_pengantar_fileName = str_random(5)."-".date('his')."-".str_random(3).".".$memo_pengantar_extension;
          $memo_pengantar_folderpath  = 'storage/upload/memo_pengantar'.'/';
          $memo_pengantar->move($memo_pengantar_folderpath , $memo_pengantar_fileName);
        }

        if($request->file('doc_lka')!=''){ 
          $input = $this->validate(request(), [
             'doc_lka' => 'required|mimes:pdf',
          ]);
          $doc_lka = $request->file('doc_lka');
          $doc_lka_extension = $doc_lka->getClientOriginalExtension();
          $doc_lka_fileName = str_random(5)."-".date('his')."-".str_random(3).".".$doc_lka_extension;
          $doc_lka_folderpath  = 'storage/upload/doc_lka'.'/';
          $doc_lka->move($doc_lka_folderpath , $doc_lka_fileName);
        }

        if($request->file('doc_cas')!=''){ 
          $input = $this->validate(request(), [
             'doc_cas' => 'required|mimes:pdf',
          ]);
          $doc_cas = $request->file('doc_cas');
          $doc_cas_extension = $doc_cas->getClientOriginalExtension();
          $doc_cas_fileName = str_random(5)."-".date('his')."-".str_random(3).".".$doc_cas_extension;
          $doc_cas_folderpath  = 'storage/upload/doc_cas'.'/';
          $doc_cas->move($doc_cas_folderpath , $doc_cas_fileName);
        }

        if($request->file('doc_bg')!=''){ 
          $input = $this->validate(request(), [
             'doc_bg' => 'required|mimes:pdf',
          ]);
          $doc_bg = $request->file('doc_bg');
          $doc_bg_extension = $doc_bg->getClientOriginalExtension();
          $doc_bg_fileName = str_random(5)."-".date('his')."-".str_random(3).".".$doc_bg_extension;
          $doc_bg_folderpath  = 'storage/upload/doc_bg'.'/';
          $doc_bg->move($doc_bg_folderpath , $doc_bg_fileName);
        }

        if($request->file('doc_pml')!=''){ 
          $input = $this->validate(request(), [
             'doc_pml' => 'required|mimes:pdf',
          ]);
          $doc_pml = $request->file('doc_pml');
          $doc_pml_extension = $doc_pml->getClientOriginalExtension();
          $doc_pml_fileName = str_random(5)."-".date('his')."-".str_random(3).".".$doc_pml_extension;
          $doc_pml_folderpath  = 'storage/upload/doc_pml'.'/';
          $doc_pml->move($doc_pml_folderpath , $doc_pml_fileName);
        }

        if($request['pembayaran']=='Lainnya'){
            $pembayaran = $request['pembayaran_lain'];
        }else{
          $pembayaran = $request['pembayaran'];
        }

        if($request['jaminan']=='Lainnya'){
            $jaminan = $request['jaminan_lain'];
        }else{
          $jaminan = $request['jaminan'];
        }
        if($request['syarat_penyerahan']=='Lainnya'){
            $syarat_penyerahan = $request['syarat_penyerahan_lain'];
        }else{
          $syarat_penyerahan = $request['syarat_penyerahan'];
        }
        if($request['lama_tempo']=='Lainnya'){
          $lama_tempo = $request['lama_tempo_lain'];
        }else{
          $lama_tempo = $request['lama_tempo'];
        }

        $CA = Kredit::find($id);
        $CA->no_surat = $request->get('no_surat');
        $CA->customer_id = $request->get('customer');
        $CA->volume = str_replace('.','', $request->get('volume'));
        $CA->satuan = $request->get('satuan');
        $CA->periode_volume = $request->get('periode_volume');
        $CA->nilai_transaksi = str_replace('.','', $request->get('nilai_transaksi'));
        $CA->credit_limit = str_replace('.','', $request->get('credit_limit'));
        $CA->pembayaran = $pembayaran;
        $CA->jaminan = $jaminan;
        $CA->periode_penyerahan = $request->get('periode_penyerahan');
        $CA->syarat_penyerahan = $syarat_penyerahan;
        $CA->flag_denda = $request->get('flag_denda');
        $CA->tempo_start = $request->get('tempo_start');
        $CA->tempo_end = $request->get('tempo_end');
        $CA->lama_tempo = $lama_tempo;
        
        if($request->file('memo_pengantar')!=''){ 
          $CA->memo_pengantar = $memo_pengantar_folderpath.$memo_pengantar_fileName;
        }

        if($request->file('doc_lka')!=''){ 
          $CA->doc_lka = $doc_lka_folderpath.$doc_lka_fileName;
        }

        if($request->file('doc_cas')!=''){ 
          $CA->doc_cas = $doc_cas_folderpath.$doc_cas_fileName;
        } 

        if($request->file('doc_bg')!=''){ 
          $CA->doc_bg = $doc_bg_folderpath.$doc_bg_fileName;
        }

        if($request->file('doc_pml')!=''){ 
          $CA->doc_pml = $doc_pml_folderpath.$doc_pml_fileName;
        }       
  
        $CA->save();

        DB::table('tr_ca_product')->where('credit_approval_id', $id)->delete();

        foreach ($request['product'] as $key => $value) {
            $ca_product=CaProduct::create([
              'credit_approval_id' => $id,
              'product_id' => $value
            ]);
        }

        // $this->sendEmail($request->get('customer'), 3, $id);
        return redirect('Kredit/'.$CA->id.'')->with('success','Kredit Berhasil Diubah');
    }

    public function submit(Request $request, $id, $status)
    {
      $datetime = date("Y-m-d H:i:s");

        $kredit = Kredit::find($id);
        $kredit->status = $status;

        // Timestamps
        if (Auth::user()->isRole('ar')) {
          $kredit->submit_ar_date = $datetime;
        }elseif (Auth::user()->isRole('cashbank')) {
          $kredit->submit_cb_date = $datetime;
        }elseif (Auth::user()->isRole('fbs')) {
          $kredit->submit_fbs_date = $datetime;
        }
        
        $kredit->save();

        $this->sendEmail($kredit->customer_id, (Auth::user()->RoleUser->role_id+1), $id);
        
       return redirect('Kredit')->with('success',''.$kredit->Customer->name.' telah di Submit');
    }

    public function approve(Request $request, $id, $status)
    {
      $datetime = date("Y-m-d H:i:s");
        $kredit = Kredit::find($id);
        $kredit->status = $status;

        // Note
          CaActionNote::create([
            'credit_approval_id' => $id,
            'action_note' => $request['note'],
            'action_type' => '1', //Approve Action
            'action_by' => Auth::user()->id
          ]);

        // Timestamps
        if (Auth::user()->isRole('ar')) {
          $kredit->submit_ar_date = $datetime;
        }elseif (Auth::user()->isRole('cashbank')) {
          $kredit->submit_cb_date = $datetime;
        }elseif (Auth::user()->isRole('fbs')) {
          $kredit->submit_fbs_date = $datetime;
        }elseif (Auth::user()->isRole('manajemenresiko')) {
          $kredit->submit_mr_date = $datetime;
        }elseif (Auth::user()->isRole('komitekredit')) {
          $kredit->submit_kk_date = $datetime;
        }

        $kredit->save();

        $this->sendEmail($kredit->customer_id, (Auth::user()->RoleUser->role_id+1), $id);
       return redirect('Kredit')->with('success',''.$kredit->no_tiket.' atas nama '.$kredit->Customer->name.' telah di Submit');
    }

    public function reject(Request $request, $id, $status)
    {
        $kredit = Kredit::find($id);
        $kredit->status = $status;

        // Note
        if ($request['note']!='' OR $request['note']!=null) {
            $datetime = date("Y-m-d H:i:s");
              CaActionNote::create([
                'credit_approval_id' => $id,
                'action_note' => $request['note'],
                'action_type' => '0', //Reject Action
                'action_by' => Auth::user()->id
              ]);
        }

        $kredit->save();
       return redirect('Kredit')->with('success',''.$kredit->no_tiket.' telah di reject');
    }

    public function show($id)
    {
        $kredit = Kredit::find($id);
        return view('Kredit.show', compact('kredit'));
    }

     public function destroy($id)
    {
        $card = Kredit::find($id);
        $card->delete();
        return redirect('Kredit')->with('success','Kredit telah di hapus');
    }

    public function showPdf(Request $request, $id) {
        $kredit = Kredit::find($id);
        $ttd = Ttd::where('document_type', '=', 'CA')->first();
        


      $jatuh_tempo = array('0001 Payable immediately Due net',
                          '0002 within 14 days 2 % cash discount',
                          '0002 within 30 days Due net',
                          '0003 within 14 days 3 % cash discount',
                          '0003 within 20 days 2 % cash discount',
                          '0003 within 30 days Due net',
                          '0004 Payable immediately Due net',
                          '0004 Baseline date on End of the month',
                          '0005 Payable immediately Due net',
                          '0005 Baseline date on 10 of next month',
                          '0006 Before End of the month 4 % cash discount',
                          '0006 Before 15 of the next month ; 2 % cash discount',
                          '0006 Before 15 in 2 months Due net',
                          '0007 For Invoicing up to 15 of Month',
                          '0007 Before 15 of the next month ; 2 % cash discount',
                          '0007 Before End of the next month ; Due net',
                          '0007 For Invoicing up to End of Month',
                          '0007 Before End of the next month ; 2 % cash discoun',
                          '0007 Before 15 in 2 months Due net',
                          '0008 For Invoicing up to 15 of Month',
                          '0008 within 14 days 2 % cash discount',
                          '0008 within 30 days 1,5 % cash discount',
                          '0008 within 45 days Due net',
                          '0008 Baseline date on 30 of the month',
                          '0008 For Invoicing up to End of Month',
                          '0008 within 14 days 2 % cash discount',
                          '0008 within 30 days 1,5 % cash discount',
                          '0008 within 45 days Due net',
                          '0008 Baseline date on 15 of next month',
                          '0009 Payable in 3 partial amounts',
                          '0009 1 installment: 30,000 % with payment term 0001',
                          '0009 2 installment: 40,000 % with payment term 0001',
                          '0009 3 installment: 30,000 % with payment term 0001',
                          'C000 Within 30 days Due net',
                          'R000 Within 1 day Due net',
                          'R001 Within 7 days Due net',
                          'R002 Within 10 days Due net',
                          'R003 Within 15 days Due net',
                          'R004 Within 20 days Due net',
                          'R005 Within 30 days Due net',
                          'R006 Before 15 of the next month ; Due net',
                          'R007 Before End in 2 months Due net',
                          'R008 Before 15 in 2 months Due net',
                          'R009 Before End of the next month ; Due net',
                          'R010 Within 90 days Due net',
                          'R011 Within 25 days Due net',
                          'R012 Within 29 days Due net',
                          'Z000 Within 6 days due net',
                          'Z001 Within 14 days due net',
                          'Z002 Within 30 days Due net',
                          'Z003 Within 8 days due net',
                          'Z004 Within 10 days due net',
                          'Z007 Within 120 days due net',
                          'Z008 Within 4 days due net',
                          'Z009 Within 40 days due net',
                          'Z010 Within 45 days due net',
                          'Z011 Within 3 days due net',
                          'Z012 Within 35 days due net',
                          'Z013 Within 5 days due net',
                          'Z014 Within 9 days due net',
                          'Z015 Within 21 days due net',
                          'Z016 Within 26 days due net',
                          'Z017 Within 23 days due net',
                          'Z018 Within 27 days due net',
                          'Z029 Within 30 days due net after BL date - count as 1',
                          'Z030 Within 180 days due net',
                          'ZC01 Within day auto collections',
                          'ZC03 Within 3 days auto collection',
                          'ZC07 Within 7 days auto collection',
                          'ZC10 Within 10 days auto collection',
                          'ZC14 Within 14 days auto collection',
                          'ZC21 Within 21 days auto collection',
                          'ZC30 Within 30 days auto collection',
                          'ZP07 Within 7 days Due net',
                          'ZP15 Within 15 days Due net',
                          'ZP30 Within 30 days Due net',
                          'ZP45 Within 45 days Due net',
                          'ZP60 Within 60 days Due net',
                          'ZP61 within 60 days due net',
                          'ZR01 Payment Term for EDC',
                          'ZR02 Payment Term for Deposit',
                          'ZR03 Credit within 4 days Due net',
                          'Lainnya');

       if(!in_array($kredit['lama_tempo'], $jatuh_tempo)){
          $dayday='Hari';
        }else{
          $dayday='';
        }

        $data = [
            'kredit'=> $kredit,
            'ttd' => $ttd,
            'dayday' => $dayday,
        ];

      view()->share('data',$data);
          $pdf = PDF::loadView('Pdf/credit-approval');
          return $pdf->download('Pdf/credit-approval.pdf')
                     ->header('Content-Type', 'application/pdf');
        //return view('Pdf/credit-approval');
    }

    public function showPdfNota(Request $request, $id) {
        $kredit = Kredit::find($id);
        $nota = CaNota::where('credit_approval_id', '=', $id)->first();
        $data = [
            'kredit'=> $kredit,
            'nota' => $nota,
        ];

      view()->share('data',$data);
          $pdf = PDF::loadView('Pdf/nota');
          return $pdf->download('Pdf/nota.pdf')
                     ->header('Content-Type', 'application/pdf');
    }

    public function sendEmail($customer_id, $user_role, $kredit) {
        $kredit=Kredit::find($kredit);
        $customer=Customer::find($customer_id);
        $user=User::whereHas('RoleUser', function($q) use($user_role){
                $q->where('role_id', $user_role);
            })->get();
        $data = $kredit;


        if ($customer->email) {
          Mail::to($customer->email)->send(new SendMailable($data));
        }

        foreach ($user as $key) {
          Mail::to($key->email)->send(new SendMailable($data));
        }

    }

    // Account Receivable (AR)

    public function ArForm($id) {
        $kredit = Kredit::find($id);

        return view('Kredit/Ar.form', compact('kredit'));
    }

    public function ArSave(Request $request, $id) {

        $input = $this->validate(request(), [
           'customer' => 'required',
           'product' => 'required',
           'volume' => 'required',
           'nilai_transaksi' => 'required',
           'credit_limit' => 'required',
           'pembayaran' => 'required',
           'jaminan' => 'required',
           'syarat_penyerahan' => 'required',
           'tempo_start' => 'required',
           'tempo_end' => 'required',
           'menu_pengantar' => 'required|mimes:pdf',
           'doc_lka' => 'required|mimes:pdf'
        ]);

        $menu_pengantar = $request->file('menu_pengantar');
        $menu_pengantar_extension = $menu_pengantar->getClientOriginalExtension();
        $menu_pengantar_fileName = str_random(5)."-".date('his')."-".str_random(3).".".$menu_pengantar_extension;
        $menu_pengantar_folderpath  = 'storage/upload/menu_pengantar'.'/';
        $menu_pengantar->move($menu_pengantar_folderpath , $menu_pengantar_fileName);

        $doc_lka = $request->file('doc_lka');
        $doc_lka_extension = $doc_lka->getClientOriginalExtension();
        $doc_lka_fileName = str_random(5)."-".date('his')."-".str_random(3).".".$doc_lka_extension;
        $doc_lka_folderpath  = 'storage/upload/doc_lka'.'/';
        $doc_lka->move($doc_lka_folderpath , $doc_lka_fileName);

          $Kredit=Kredit::create([
            'no_tiket' => date('his'),
            'customer_id' => $request['customer'],
            'volume' => $request['volume'],
            'nilai_transaksi' => $request['nilai_transaksi'],
            'credit_limit' => $request['credit_limit'],
            'pembayaran' => $request['pembayaran'],
            'jaminan' => $request['jaminan'],
            'syarat_penyerahan' => $request['syarat_penyerahan'],
            'flag_denda' => $request['flag_denda'],
            'tempo_start' => $request['tempo_start'],
            'tempo_end' => $request['tempo_end'],
            'menu_pengantar' => $menu_pengantar_folderpath.$menu_pengantar_fileName,
            'doc_lka' => $doc_lka_folderpath.$doc_lka_fileName,
            'created_by' => Auth::user()->id,
          ]);
          
        return redirect('Kredit')->with('success','Kredit Berhasil Diajukan');
    }
}
