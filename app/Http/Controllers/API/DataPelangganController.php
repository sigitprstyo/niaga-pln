<?php

namespace App\Http\Controllers\API;

use App\Models\Master\DataPelanggan;
use App\Models\Transaksi\DataPelangganImage;
use App\Models\Transaksi\Point;
use App\Models\Transaksi\Deal;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class DataPelangganController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data_pelanggan = DataPelanggan::with(['User','Photo'])->whereDoesntHave('Deal')
                                            ->where('created_by', auth()->user()->id)->get();

            return $data_pelanggan;

        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => $th
            ], 422);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $this->validate(request(), [
                'no_ktp' => 'required',
                'nama_pelanggan' => 'required',
                'tarif_baru' => 'required',
                'daya_baru' => 'required',
                'type' => 'required',
                'no_agenda' => 'required',
                'no_hp' => 'required',
            ]);

            DB::beginTransaction();

            $customer['no_ktp'] = $request['no_ktp'];
            $customer['nama_pelanggan'] = $request['nama_pelanggan'];
            $customer['tarif'] = $request['tarif_baru'];
            $customer['daya'] = $request['daya_baru'];
            $customer['koordinat_x'] = $request['koordinat_x'];
            $customer['koordinat_y'] = $request['koordinat_y'];
            $customer['is_probing_individual'] = $request['is_probing_individual'];
            $customer['created_by'] = auth()->user()->id;
            $customer['type'] = $request['type'];
            $customer['tarif_lama'] = $request['tarif_lama'];
            $customer['daya_lama'] = $request['daya_lama'];
            $customer['merek'] = $request['merek'];
            $customer['kapasitas'] = $request['kapasitas'];
            $customer['qty_unit'] = $request['qty_unit'];
            $customer['kelompok'] = $request['kelompok'];
            $customer['no_agenda'] = $request['no_agenda'];
            $customer['no_hp'] = $request['no_hp'];

            // Create new customer
            $customeradd = DataPelanggan::create($customer);
            
            $type = $request['type'];
            $kredit = 0;

            switch ($type) {
                case 'PB':
                    $tarif = $request['daya_baru'];
                    $kredit = round($tarif/500);

                    break;
                
                case 'PD':
                    $selisih = $request['daya_baru'] - $request['daya_lama'];
            
                    $kredit = round($selisih/500);

                    break;
                    
                default:
                    # code...
                    break;
            }

            // Insert to point table
            $point['tipe_transaksi'] = 'probing';
            $point['kredit'] = $kredit;
            $point['keterangan'] = 'Probing input';
            $point['is_approve_point'] = false;
            $point['id_pelanggan'] = $customeradd->id;
            $point['id_user'] = auth()->user()->id;
            Point::create($point);
            
            if ($customeradd) {
                DB::commit();
                return response($customeradd, 200);
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'status' => 'error',
                'message' => $th
            ], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return DataPelanggan::with(['User','Photo'])->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $customer = DataPelanggan::find($id);
            $customer['no_ktp'] = $request['no_ktp'];
            $customer['nama_pelanggan'] = $request['nama_pelanggan'];
            $customer['tarif'] = $request['tarif'];
            $customer['daya'] = $request['daya'];
            $customer['data_pembangkit'] = $request['data_pembangkit'];
            $customer['koordinat_x'] = $request['koordinat_x'];
            $customer['koordinat_y'] = $request['koordinat_y'];
            $customer['is_probing_individual'] = $request['is_probing_individual'];
            $customer['created_by'] = auth()->user()->id;

            $customer->save();

            return DataPelanggan::with('User')->find($id);
        } catch (\Throwable $th) {
            return $th;
            // return response("Failed", 402);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DataPelanggan::find($id)->delete();
            return "Success...";
        } catch (\Throwable $th) {
            return $th;
            // return response("Failed", 402);
        }
    }

    public function store_image(Request $request, $id){
        $probing = DataPelanggan::findOrFail($id);

        $image = $request->file('image')->store('public/upload/images');
        $path = str_replace("public/","storage/", $image);

        $photo['id_pelanggan'] = $probing->id;
        $photo['path_foto'] = $path;
        $customerimage = DataPelangganImage::create($photo);

        return $customerimage;
    }

    public function list_pending_probing()
    {

        return DataPelanggan::with('User')->where('is_rejected', 0)->where('is_approved', 0)->where('created_by', auth()->user()->id)->get();
    }
}
