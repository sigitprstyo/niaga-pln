<?php

namespace App\Http\Controllers\API;

use App\Models\Master\Reward;
use App\Models\Transaksi\Point;
use App\Models\Transaksi\Deal;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PointController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $kredit_point = Point::where('id_user', auth()->user()->id)->where('is_approve_point', 1)->where('is_reject_point', 0)->sum('kredit');
            $debit_point = Point::where('id_user', auth()->user()->id)->where('is_approve_point', 1)->where('is_reject_point', 0)->sum('debit');
            $total_point = $kredit_point - $debit_point;

            return $total_point;

        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => $th
            ], 422);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function history_list()
    {
        try {
            $list_point = Point::with(['Reward'])->where('tipe_transaksi', 'reward')
                                    ->where('id_user', auth()->user()->id)->get();

            return $list_point;

        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => $th
            ], 422);
        }
    }

    public function list_rejected()
    {

        try {
            $list_point = Point::with(['User','Pelanggan','Deal'])->where('tipe_transaksi', '<>', 'reward')
                                ->where('is_reject_point', 1)->where('id_user', auth()->user()->id)->get();

            return $list_point;

        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => $th
            ], 422);
        }
    }

    public function show($id)
    {
        return Point::with(['User','Photo'])->find($id);
    }
}
