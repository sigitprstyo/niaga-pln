<?php

namespace App\Http\Controllers\API;

use App\Models\Master\DataPelanggan;
use App\Models\Transaksi\Deal;
use App\Models\Transaksi\Point;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class DealController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Deal::with(['Pelanggan', 'User'])->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $this->validate(request(), [
                'id_pelanggan' => 'required',
            ]);
            
            DB::beginTransaction();

            // Validate if id_pelanggan exist in DataPelanggan
            $probing = DataPelanggan::find($request['id_pelanggan']);

            $deal['id_pelanggan'] = $probing->id;
            $deal['no_agenda'] = ($probing->no_agenda ? $probing->no_agenda : "-");
            $deal['is_tmp'] = 1;//$request['is_tmp'];
            $deal['id_user'] = auth()->user()->id;

            // Create new deal
            $dealadd = Deal::create($deal);
            $kredit = 0;

            switch ($probing->type) {
                case 'PB':
                    $kredit = round($probing->daya/50);

                    break;
                
                case 'PD':

                    $selisih = $probing->daya - $probing->daya_lama;
                    $kredit = round($selisih/100);

                    break;
                    
                default:
                    # code...
                    break;
            }

            // Insert to point table
            $point['tipe_transaksi'] = 'deal';
            $point['kredit'] = $kredit;
            $point['keterangan'] = 'PDL/Deal input';
            $point['is_approve_point'] = false;
            $point['id_pelanggan'] = $deal['id_pelanggan'];
            $point['id_deal'] = $dealadd->id;
            $point['id_user'] = auth()->user()->id;
            Point::create($point);

            if ($dealadd) {
                DB::commit();
                return response($dealadd, 200);
            }

        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'status' => 'error',
                'message' => $th
            ], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Deal::with(['Pelanggan', 'User'])->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $deal = Deal::find($id);
            $deal['id_pelanggan'] = $request['id_pelanggan'];
            $deal['no_agenda'] = $request['no_agenda'];
            $deal['is_tmp'] = $request['is_tmp'];
            $deal['id_user'] = auth()->user()->id;

            $deal->save();

            return Deal::with(['Pelanggan', 'User'])->find($id);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => $th
            ], $th->status);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Deal::find($id)->delete();
            return response()->json([
                'status' => 'Success',
                'message' => 'Success...'
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => $th
            ], $th->status);
        }
    }

    public function list_pending_closing()
    {
        return Deal::with(['User','Pelanggan'])->where('is_rejected', 0)->where('is_approved', 0)->where('id_user', auth()->user()->id)->get();
    }
}
