<?php

namespace App\Http\Controllers\API;

use App\Models\Master\Reward;
use App\Models\Transaksi\Point;
use App\Models\Transaksi\Deal;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RewardApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data = Reward::get();

            return $data;

        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => $th
            ], 422);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $this->validate(request(), [
                'id_reward' => 'required',
            ]);

            $kredit_point = Point::where('id_user', auth()->user()->id)->where('is_approve_point', 1)->where('is_reject_point', 0)->sum('kredit');
            $debit_point = Point::where('id_user', auth()->user()->id)->where('is_approve_point', 1)->where('is_reject_point', 0)->sum('debit');
            $total_point = $kredit_point - $debit_point;

            $reward = Reward::find($request['id_reward']);

            // if($reward->point > $total_point){
            //     throw new Exception("Point anda tidak cukup");
            // }

            $point['tipe_transaksi'] = 'reward';
            $point['debit'] = $reward->point;
            $point['keterangan'] = 'redeem reward';
            $point['is_approve_point'] = false;
            $point['id_user'] = auth()->user()->id;
            $point['id_reward'] = $reward->id;
            $redeem = Point::create($point);

            
            if ($redeem) {
                return response($redeem, 200);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => $th
            ], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return DataPelanggan::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $customer = DataPelanggan::find($id);
            $customer['no_ktp'] = $request['no_ktp'];
            $customer['nama_pelanggan'] = $request['nama_pelanggan'];
            $customer['tarif'] = $request['tarif'];
            $customer['daya'] = $request['daya'];
            $customer['data_pembangkit'] = $request['data_pembangkit'];
            $customer['koordinat_x'] = $request['koordinat_x'];
            $customer['koordinat_y'] = $request['koordinat_y'];
            $customer['is_probing_individual'] = $request['is_probing_individual'];
            $customer['created_by'] = auth()->user()->id;

            $customer->save();

            return DataPelanggan::with('User')->find($id);
        } catch (\Throwable $th) {
            return $th;
            // return response("Failed", 402);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DataPelanggan::find($id)->delete();
            return "Success...";
        } catch (\Throwable $th) {
            return $th;
            // return response("Failed", 402);
        }
    }

    public function list_reward($id)
    {
        try {
            $data = Reward::get();

            return $data;

        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => $th
            ], 422);
        }
    }
}
