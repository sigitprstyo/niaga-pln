<?php

namespace App\Http\Controllers\API;

use App\User;
use App\Models\Master\Tarif;
use App\Models\Master\KelompokTarif;
use App\Models\Master\Daya;
use App\Models\Master\Iklan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PickerController extends Controller
{
    public function showTarif()
    {

        return Tarif::get();

    }

    public function showDaya()
    {

        return Daya::get();

    }

    public function showKelompokTarif()
    {

        return KelompokTarif::get();

    }

    public function showPromo()
    {
        $iklan = Iklan::get();

        return $iklan;

    }
}
