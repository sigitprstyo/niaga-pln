<?php return array (
  'sans-serif' => array(
    'normal' => $rootDir . '\lib\fonts\Helvetica',
    'bold' => $rootDir . '\lib\fonts\Helvetica-Bold',
    'italic' => $rootDir . '\lib\fonts\Helvetica-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $rootDir . '\lib\fonts\Times-Roman',
    'bold' => $rootDir . '\lib\fonts\Times-Bold',
    'italic' => $rootDir . '\lib\fonts\Times-Italic',
    'bold_italic' => $rootDir . '\lib\fonts\Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $rootDir . '\lib\fonts\Times-Roman',
    'bold' => $rootDir . '\lib\fonts\Times-Bold',
    'italic' => $rootDir . '\lib\fonts\Times-Italic',
    'bold_italic' => $rootDir . '\lib\fonts\Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $rootDir . '\lib\fonts\Courier',
    'bold' => $rootDir . '\lib\fonts\Courier-Bold',
    'italic' => $rootDir . '\lib\fonts\Courier-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $rootDir . '\lib\fonts\Helvetica',
    'bold' => $rootDir . '\lib\fonts\Helvetica-Bold',
    'italic' => $rootDir . '\lib\fonts\Helvetica-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $rootDir . '\lib\fonts\ZapfDingbats',
    'bold' => $rootDir . '\lib\fonts\ZapfDingbats',
    'italic' => $rootDir . '\lib\fonts\ZapfDingbats',
    'bold_italic' => $rootDir . '\lib\fonts\ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $rootDir . '\lib\fonts\Symbol',
    'bold' => $rootDir . '\lib\fonts\Symbol',
    'italic' => $rootDir . '\lib\fonts\Symbol',
    'bold_italic' => $rootDir . '\lib\fonts\Symbol',
  ),
  'serif' => array(
    'normal' => $rootDir . '\lib\fonts\Times-Roman',
    'bold' => $rootDir . '\lib\fonts\Times-Bold',
    'italic' => $rootDir . '\lib\fonts\Times-Italic',
    'bold_italic' => $rootDir . '\lib\fonts\Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $rootDir . '\lib\fonts\Courier',
    'bold' => $rootDir . '\lib\fonts\Courier-Bold',
    'italic' => $rootDir . '\lib\fonts\Courier-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $rootDir . '\lib\fonts\Courier',
    'bold' => $rootDir . '\lib\fonts\Courier-Bold',
    'italic' => $rootDir . '\lib\fonts\Courier-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $rootDir . '\lib\fonts\DejaVuSans-Bold',
    'bold_italic' => $rootDir . '\lib\fonts\DejaVuSans-BoldOblique',
    'italic' => $rootDir . '\lib\fonts\DejaVuSans-Oblique',
    'normal' => $rootDir . '\lib\fonts\DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $rootDir . '\lib\fonts\DejaVuSansMono-Bold',
    'bold_italic' => $rootDir . '\lib\fonts\DejaVuSansMono-BoldOblique',
    'italic' => $rootDir . '\lib\fonts\DejaVuSansMono-Oblique',
    'normal' => $rootDir . '\lib\fonts\DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $rootDir . '\lib\fonts\DejaVuSerif-Bold',
    'bold_italic' => $rootDir . '\lib\fonts\DejaVuSerif-BoldItalic',
    'italic' => $rootDir . '\lib\fonts\DejaVuSerif-Italic',
    'normal' => $rootDir . '\lib\fonts\DejaVuSerif',
  ),
  'material icons' => array(
    'normal' => $fontDir . '\187ca9e4fe4e35b61d160108ae83ae9e',
  ),
  'glyphicons halflings' => array(
    'normal' => $fontDir . '\3028caee5615b27b169f342a8ce7d4f6',
  ),
  'roboto' => array(
    'normal' => $fontDir . '\bb4b20932b2f9a465d390883d27fe157',
    'italic' => $fontDir . '\f05ab86ee3bc5c2d15e6165a8b3eb23f',
  ),
  'roboto-thin' => array(
    'normal' => $fontDir . '\bb4b20932b2f9a465d390883d27fe157',
  ),
  'roboto-thinitalic' => array(
    'normal' => $fontDir . '\f05ab86ee3bc5c2d15e6165a8b3eb23f',
  ),
  'roboto-light' => array(
    'normal' => $fontDir . '\2ba7ab53e0290233e3963a16e85c40e2',
  ),
  'roboto-lightitalic' => array(
    'normal' => $fontDir . '\d355e8868a8d8670822dd8697af407b8',
  ),
  'roboto-regular' => array(
    'normal' => $fontDir . '\1b45a665a9842cd3c34f07e1aa0262c0',
  ),
  'roboto-regularitalic' => array(
    'normal' => $fontDir . '\1221729e0bb31a7caf41cebaf86e5e21',
  ),
  'roboto-medium' => array(
    'normal' => $fontDir . '\1055816dd6287b478c63278e6d6607ad',
  ),
  'roboto-mediumitalic' => array(
    'normal' => $fontDir . '\97d85b7facb0d11274e81a3e69fd3224',
  ),
  'roboto-bold' => array(
    'normal' => $fontDir . '\84e5e3d254c638bf9232282866632e7b',
  ),
  'roboto-bolditalic' => array(
    'normal' => $fontDir . '\cf4be18bc1c4fd6cee0145a2a505f7f5',
  ),
  'roboto-black' => array(
    'normal' => $fontDir . '\0fe17f2c36139ec8a7b333d5e2d64ea8',
  ),
  'roboto-blackitalic' => array(
    'normal' => $fontDir . '\aebe0f755154842d033bd46c65292f3e',
  ),
) ?>