<?php

use Hash;
use Illuminate\Http\Request;

Route::post('/login', function (Request $request) {


    $request->validate([
        'email' => 'required|email',
        'password' => 'required'
    ]);

    if (Auth::attempt($request->only('email', 'password'))) {

        $user = Auth::user();
        $user->api_token = str_random(100);
        $user->save();
        
        return $user;
    } else {
        return response()->json([
            'status' => 'error',
            'message' => 'Wrong username and password'
        ], 401);
    }
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::resource('probing', 'API\DataPelangganController');
    Route::post('probing/images/{id}', 'API\DataPelangganController@store_image');
    Route::get('list/probing', 'API\DataPelangganController@list_pending_probing');

    Route::resource('deal', 'API\DealController');
    Route::get('list/closing', 'API\DealController@list_pending_closing');

    Route::resource('reward', 'API\RewardApiController');
  
    Route::get('/pickerTarif', 'Api\PickerController@showTarif');
    Route::get('/pickerDaya', 'Api\PickerController@showDaya');
    Route::get('/pickerKelompokTarif', 'Api\PickerController@showKelompokTarif');
    Route::get('/promo', 'Api\PickerController@showPromo');

    Route::get('/point/{id}', 'Api\PointController@show');
    Route::get('/point', 'Api\PointController@index');
    Route::get('/history/point', 'Api\PointController@history_list');
    Route::get('/list/rejected', 'API\PointController@list_rejected');

    Route::get('/ceklogin', function (Request $request) {
        $user = Auth::user();
        return $user;
    });

    Route::post('/changepassword', function (Request $request) {

        try {
            $request->validate([
                'old_pass' => 'required',
                'new_pass' => 'required|min:5|confirmed',
            ]);
    
            if (!(Hash::check($request->get('old_pass'), Auth::user()->password))) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Your current password does not matches with the password you provided. Please try again.'
                ], 422);
            }
    
            if(strcmp($request->get('old_pass'), $request->get('new_pass')) == 0){
                return response()->json([
                    'status' => 'error',
                    'message' => 'New Password cannot be same as your current password. Please choose a different password.'
                ], 422);
            }
            
            $new_pass = $request->get('new_pass');

            $user = Auth::user();
            $user->password = bcrypt($new_pass);
            $user->save();

            return $user;

        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => $th
            ], 422);
        }
    });
});
