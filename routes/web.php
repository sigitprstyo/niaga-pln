<?php

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Master
Route::resource('/Master/Reward', 'Master\RewardController');
Route::post('/Master/Reward/Update/{id}', 'Master\RewardController@update');

Route::resource('/Master/KelompokTarif', 'Master\KelompokTarifController');
Route::resource('/Master/Tarif', 'Master\TarifController');
Route::resource('/Master/Daya', 'Master\DayaController');
Route::resource('/Master/Iklan', 'Master\IklanController');
Route::resource('/Master/User', 'Master\UserController');
Route::post('/Master/User/Update/{id}', 'Master\UserController@update');

// Customer
Route::resource('/Master/Customer', 'Master\CustomerController');
Route::post('/Master/Customer/Update/{id}', 'Master\CustomerController@update');
Route::post('Master/Customer/Export', 'Master\CustomerController@export');
Route::post('Master/Customer/UploadUpdate', 'Master\CustomerController@UploadUpdate');

// Product
Route::resource('/Master/Product', 'Master\ProductController');
Route::post('/Master/Product/Update/{id}', 'Master\ProductController@update');
Route::post('Master/Product/Export', 'Master\ProductController@export');
Route::post('Master/Product/UploadUpdate', 'Master\ProductController@UploadUpdate');

// Ttd
Route::resource('/Master/Ttd', 'Master\TtdController');
Route::post('/Master/Ttd/Update/{id}', 'Master\TtdController@update');
Route::post('Master/Ttd/Export', 'Master\TtdController@export');
Route::post('Master/Ttd/UploadUpdate', 'Master\TtdController@UploadUpdate');

// Route::get('export/customer', 'Master\CustomerController@export_customerlist');

// Kredit
Route::resource('/Kredit', 'Kredit\KreditController');
Route::post('/Kredit/filter', 'Kredit\KreditController@filter');
Route::get('/Kredit/list/expired', 'Kredit\KreditController@expired');
Route::post('/Kredit/user/submit/{id}/{status}', 'Kredit\KreditController@submit');
Route::post('/Kredit/Update/{id}', 'Kredit\KreditController@update');
// Route::get('/Kredit/ar/form/{id}', 'Kredit\KreditController@ArForm');
// Route::get('/Kredit/ar/save/{id}', 'Kredit\KreditController@ArSave');

// Customer Detail
Route::resource('/Kredit/AR', 'Kredit\CustomerDetailController');
Route::get('/Kredit/ar/form/{id}', 'Kredit\CustomerDetailController@add');
Route::post('/Kredit/ar/save/{id}', 'Kredit\CustomerDetailController@save');
Route::post('/Kredit/ar/update/{id}', 'Kredit\CustomerDetailController@update');
Route::post('/Kredit/ar/submit/{id}', 'Kredit\CustomerDetailController@submit');

//Cash Bank
Route::get('/Kredit/cashbank/form/{id}', 'Kredit\CustomerDetailController@upload');
Route::post('/Kredit/cashbank/save/{id}', 'Kredit\CustomerDetailController@upload_save');
Route::get('/Kredit/cashbank/edit/{id}', 'Kredit\CustomerDetailController@cb_edit');
Route::post('/Kredit/cashbank/update/{id}', 'Kredit\CustomerDetailController@cb_update');

//Finance Business Support
Route::get('/Kredit/Fbs/entry/{id}', 'Kredit\NotaController@entry');
Route::post('/Kredit/Fbs/nota_save/{id}', 'Kredit\NotaController@nota_save');
Route::get('/Kredit/fbs/edit/{id}', 'Kredit\CustomerDetailController@fbs_edit');
Route::post('/Kredit/fbs/update/{id}', 'Kredit\CustomerDetailController@fbs_update');

Route::get('/Kredit/Fbs/form/{id}', 'Kredit\CustomerDetailController@form_cs');
Route::post('/Kredit/Fbs/save/{id}', 'Kredit\CustomerDetailController@cs_save');

//Manajemen Resiko
Route::post('/Kredit/Mr/reject/{id}/{status}', 'Kredit\KreditController@reject');
Route::post('/Kredit/Mr/approve/{id}/{status}', 'Kredit\KreditController@approve');

//Komite Kredit
Route::get('/Kredit/kk/form/{id}', 'Kredit\CustomerDetailController@kk_upload');
Route::get('/Kredit/kk/edit/{id}', 'Kredit\CustomerDetailController@kk_edit');
Route::post('/Kredit/kk/upload_save/{id}', 'Kredit\CustomerDetailController@kk_upload_save');
Route::post('/Kredit/kk/kk_update/{id}', 'Kredit\CustomerDetailController@kk_update');

//PDF Route
Route::get('Pdf/CreditApproval/{id}',array('as'=>'credit-approval','uses'=>'Kredit\KreditController@showPdf'));
Route::get('Pdf/Nota/{id}',array('as'=>'nota','uses'=>'Kredit\KreditController@showPdfNota'));
Route::get('Pdf/IdentitasCustomer/{id}',array('as'=>'IdentitasCustomer','uses'=>'Kredit\CustomerDetailController@showPdfIdentitasCustomer'));

// Emails
Route::get('/send/email/{id}', 'HomeController@mail');
Route::get('/view/email/{id}', 'HomeController@mailView');

// Approval
Route::get('/Approval/Probing', 'Kredit\ApprovalController@pending_probing');
Route::get('/Approval/Probing/{id}', 'Kredit\ApprovalController@form_probing_approval');
Route::post('/Approval/Probing/{id}', 'Kredit\ApprovalController@process_probing');
Route::get('/Approval/ProbingReject', 'Kredit\ApprovalController@reject_probing');

Route::get('/Approval/Closing', 'Kredit\ApprovalController@pending_closing');
Route::get('/Approval/Closing/{id}', 'Kredit\ApprovalController@form_closing_approval');
Route::post('/Approval/Closing/{id}', 'Kredit\ApprovalController@process_closing');
Route::post('/Approval/Verifikasi/{id}', 'Kredit\ApprovalController@verify_closing');
Route::get('/Approval/ClosingReject', 'Kredit\ApprovalController@reject_closing');

Route::get('/Approval/Redeem', 'Kredit\ApprovalController@pending_redeem');
Route::get('/Approval/Redeem/{id}', 'Kredit\ApprovalController@form_redeem_approval');
Route::post('/Approval/Redeem/{id}', 'Kredit\ApprovalController@process_redeem');
Route::get('/Approval/RedeemReject', 'Kredit\ApprovalController@reject_redeem');

Route::get('/Approval/AutoApproval', 'Kredit\ApprovalController@form_auto_approval');
Route::post('/Approval/AutoApproval', 'Kredit\ApprovalController@process_auto');

// Rekap
Route::get('/Rekap/Pending', 'Master\RekapController@rekap_pending');
Route::get('/Rekap/Submitted', 'Master\RekapController@rekap_submitted');
Route::get('/Rekap/Completed', 'Master\RekapController@rekap_completed');
Route::get('/Rekap/Rejected', 'Master\RekapController@rekap_rejected');
Route::get('/Rekap/PasangBaru', 'Master\RekapController@pasang_baru');
Route::get('/Rekap/PerubahanDaya', 'Master\RekapController@perubahan_daya');
Route::get('/Rekap/ProbingClosing', 'Master\RekapController@probing_closing');