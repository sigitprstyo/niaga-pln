<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataPelanggansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_pelanggan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_ktp')->unique();
            $table->string('nama_pelanggan');
            $table->string('tarif');
            $table->integer('daya');
            $table->integer('data_pembangkit')->nullable();
            $table->string('koordinat_x')->nullable();
            $table->string('koordinat_y')->nullable();
            $table->tinyInteger('is_probing_individual');
            $table->string('path_foto')->nullable();
            $table->string('path_ktp')->nullable();
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_pelanggan');
    }
}
