<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDataPelangganTable0312 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_pelanggan', function (Blueprint $table) {
            $table->string('merek')->nullable();
            $table->integer('kapasitas')->nullable();
            $table->integer('qty_unit')->nullable();
            $table->string('tarif_lama')->nullable();
            $table->integer('daya_lama')->nullable();
            $table->string('type')->nullable();
            $table->string('kelompok')->nullable();
            $table->string('alamat')->nullable();
            $table->string('no_agenda')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_pelanggan', function (Blueprint $table) {
            $table->dropColumn('tarif_lama');
            $table->dropColumn('daya_lama');
            $table->dropColumn('type');
            $table->dropColumn('merek');
            $table->dropColumn('kapasitas');
            $table->dropColumn('qty_unit');
            $table->dropColumn('kelompok');
            $table->dropColumn('alamat');
            $table->dropColumn('no_agenda');
        });
    }
}
