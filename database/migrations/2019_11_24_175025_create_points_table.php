<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tran_point', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipe_transaksi');
            $table->integer('kredit')->default(0);
            $table->integer('debit')->default(0);
            $table->string('keterangan')->nullable();
            $table->tinyInteger('is_approve_point')->default(0);
            $table->string('id_pelanggan')->nullable();
            $table->string('id_deal')->nullable();
            $table->string('id_reward')->nullable();
            $table->integer('id_user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tran_point');
    }
}
