<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTranDealTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tran_deal', function (Blueprint $table) {
            $table->tinyInteger('is_approved')->default(0);
            $table->tinyInteger('is_rejected')->default(0);
            $table->tinyInteger('rejected_reason')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tran_deal', function (Blueprint $table) {
            $table->dropColumn('is_approved');
            $table->dropColumn('is_rejected');
            $table->dropColumn('rejected_reason');
        });
    }
}
