<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTranDealTable2012 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tran_deal', function (Blueprint $table) {
            $table->integer('nilai_tmp')->nullable();
            $table->date('tgl_bayar')->nullable();
            $table->date('tgl_remaja')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tran_deal', function (Blueprint $table) {
            $table->dropColumn('nilai_tmp');
            $table->dropColumn('tgl_bayar');
            $table->dropColumn('tgl_remaja');
        });
    }
}
