<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaCustomerDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ca_customer_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('credit_approval_id');
            $table->string('jenis_industri');
            $table->string('keterlambatan');
            $table->string('restrukturisasi');
            $table->string('fasilitas_kredit');
            $table->string('lama_kerjasama');
            $table->string('vendor_pemasok');
            $table->string('posisi_tawar');
            $table->string('badan_usaha');
            $table->string('afiliasi');
            $table->string('kondisi_industri');
            $table->string('opini_audit');
            $table->string('audit_kap');
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ca_customer_detail');
    }
}
