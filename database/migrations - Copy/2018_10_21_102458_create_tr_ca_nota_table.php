<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrCaNotaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tr_ca_nota', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('credit_approval_id');
            $table->dateTime('tanggal_nota');
            $table->string('perihal');
            $table->text('isi');
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tr_ca_nota');
    }
}
