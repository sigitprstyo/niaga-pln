<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubmitTimeToCreditApprovalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('credit_approval', function (Blueprint $table) {
            $table->dateTime('submit_ar_date')->nullable(); // Tanggal Submit AR
            $table->dateTime('submit_cb_date')->nullable(); // Tanggal Submit Cash Bank
            $table->dateTime('submit_fbs_date')->nullable(); // Tanggal Submit Fin, Business Support
            $table->dateTime('submit_mr_date')->nullable(); // Tanggal Submit Manajemen Resiko
            $table->dateTime('submit_kk_date')->nullable(); // Tanggal Submit Komite Kredit
            $table->string('note')->nullable();
            $table->string('credit_approval_doc')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('credit_approval', function (Blueprint $table) {
            $table->dropColumn('submit_ar_date');
            $table->dropColumn('submit_cb_date');
            $table->dropColumn('submit_fbs_date');
            $table->dropColumn('submit_mr_date');
            $table->dropColumn('submit_kk_date');
            $table->dropColumn('note');
        });
    }
}
