<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrCaActionNoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tr_ca_action_note', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('credit_approval_id');
            $table->string('action_note')->nullable();     
            $table->tinyInteger('action_type'); // 0= Reject, 1= Approve
            $table->integer('action_by'); // Action Taker (User)
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tr_ca_action_note');
    }
}
