<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditApprovalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_approval', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_tiket')->unique();
            $table->string('no_surat')->unique();
            $table->integer('customer_id');
            $table->date('tempo_start');
            $table->date('tempo_end');
            $table->string('lama_tempo');
            $table->string('volume');
            $table->string('satuan');
            $table->string('periode_volume');
            $table->string('periode_penyerahan');
            $table->string('nilai_transaksi');  
            $table->string('credit_limit');
            $table->string('pembayaran');
            $table->string('jaminan');
            $table->string('memo_pengantar'); //User
            $table->string('doc_lka'); //User
            $table->string('doc_cas'); //User
            $table->string('doc_bg'); //User
            $table->string('doc_pml'); //User
            $table->string('bank_garansi_doc')->nullable(); //Cashbank
            $table->string('bank_konfirmasi_doc')->nullable(); //Cashbank
            $table->string('credit_scoring_doc')->nullable(); //Fbs
            $table->tinyInteger('flag_denda');
            $table->string('syarat_penyerahan', 10);
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('flag_read')->default(0);
            $table->tinyInteger('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_approval');
    }
}
