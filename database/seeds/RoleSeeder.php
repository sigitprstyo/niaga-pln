<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed New Role
        DB::table('roles')->insert([
            'id'          => 1,
            'name'        => 'Administrator',
            'slug'        => 'administrator',
            'description' => 'Administrator User',
            'special'     => 'all-access'
        ]);

        DB::table('roles')->insert([
            'id'          => 2,
            'name'        => 'Employee',
            'slug'        => 'employee',
            'description' => 'Employee'
        ]);

    }
}
