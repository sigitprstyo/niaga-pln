<?php

use Illuminate\Database\Seeder;
use App\Models\Master\Tarif;

class TarifSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tarif::firstOrCreate(
            [
                'tipe'     => 'R',
                'harga'  => 1000
            ]
        );

        Tarif::firstOrCreate(
            [
                'tipe'     => 'S',
                'harga'  => 1000
            ]
        );

        Tarif::firstOrCreate(
            [
                'tipe'     => 'B',
                'harga'  => 1000
            ]
        );

        Tarif::firstOrCreate(
            [
                'tipe'     => 'I',
                'harga'  => 1000
            ]
        );

        Tarif::firstOrCreate(
            [
                'tipe'     => 'P',
                'harga'  => 1000
            ]
        );
    }
}
