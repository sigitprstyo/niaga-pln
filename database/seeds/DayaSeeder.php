<?php

use Illuminate\Database\Seeder;
use App\Models\Master\Daya;

class DayaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Daya::insert([
            ['daya'=> 1300], 
            ['daya'=> 2200], 
            ['daya'=> 2400], 
            ['daya'=> 3500], 
            ['daya'=> 3900], 
            ['daya'=> 4400], 
            ['daya'=> 5500], 
            ['daya'=> 6600], 
            ['daya'=> 7700], 
            ['daya'=> 10600], 
            ['daya'=> 11000], 
            ['daya'=> 11418], 
            ['daya'=> 13200], 
            ['daya'=> 13300], 
            ['daya'=> 13500], 
            ['daya'=> 13900], 
            ['daya'=> 15400], 
            ['daya'=> 16500], 
            ['daya'=> 17600], 
            ['daya'=> 22000], 
            ['daya'=> 23000], 
            ['daya'=> 27800], 
            ['daya'=> 30000], 
            ['daya'=> 33000], 
            ['daya'=> 35200], 
            ['daya'=> 41500], 
            ['daya'=> 44000], 
            ['daya'=> 53000], 
            ['daya'=> 66000], 
            ['daya'=> 82500], 
            ['daya'=> 105000], 
            ['daya'=> 131000], 
            ['daya'=> 145500], 
            ['daya'=> 147000], 
            ['daya'=> 164000], 
            ['daya'=> 197000]
        ]);
    }
}
