<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate(
            [
                'email'     => 'admin@plnsumut.co.id',
                'password'  => bcrypt('12345678'),
                'name'      => 'Administrator',
                'api_token'      => str_random(100)
            ]
        )->assignRole(1);

        User::firstOrCreate(
            [
                'email'     => 'demo@plnsumut.co.id',
                'password'  => bcrypt('12345678'),
                'name'      => 'Demo User',
                'api_token'      => str_random(100)

            ]
        )->assignRole(2);
    }
}
