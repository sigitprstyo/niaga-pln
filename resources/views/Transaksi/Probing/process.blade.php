@extends('layouts.app')
@section('content')

  <div class="wrapper wrapper-content">
    <div class="row">
        <h2>
            <strong>
                DETAIL PROBING
            </strong>
        </h2>
    </div>

    <div class="row">
        <div class="col-lg-5">
            
            <div class="row">

                <div class="ibox-content">
                    @if (count($data_probing['Photo']) > 0)
                    <div id="gbrProbing" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            @foreach ($data_probing['Photo'] as $key => $item)
                                <li data-target="#gbrProbing" data-slide-to="{{ $key }}" class="{{ ($key == 0) ? 'active': '' }}"></li>
                            @endforeach
                        </ol>
                            
                        <div class="carousel-inner">
                            @foreach ($data_probing['Photo'] as $key => $item)
                            <div class="item {{ ($key == 0) ? 'active': '' }}">
                                @if (!empty($item->path_foto) && $item->path_foto != null)
                                <a href="{{ asset($item->path_foto) }}" title="Image from Unsplash" data-gallery="">
                                    <img src="{{ asset($item->path_foto) }}" alt="">
                                </a>
                                @else
                                <a href="{{ asset('/assets/img/no_photo.jpg') }}" title="No Imave Available" data-gallery="">
                                    <img src="{{ asset('/assets/img/no_photo.jpg') }}" alt="No Image Available">
                                </a>
                                @endif
                                
                            </div>
                            @endforeach
                        </div>
                        
                        <!-- kontrol-->
                        <a class="carousel-control left" href="#gbrProbing" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control right" href="#gbrProbing" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    @else
                    <div id="gbrProbing" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="item active">
                                <a href="{{ asset('/assets/img/no_photo.jpg') }}" title="No Imave Available" data-gallery="">
                                    <img src="{{ asset('/assets/img/no_photo.jpg') }}" alt="No Image Available">
                                </a>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>


        <div class="col-lg-7">
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tr>
                            <th>Nama Pelanggan</th>
                            <td>: &nbsp; {{ $data_probing['nama_pelanggan'] }}</td>
                        </tr>
                        <tr>
                            <th>Nomor Agenda</th>
                            <td>: &nbsp; {{ $data_probing['no_agenda'] }}</td>
                        </tr>
                        <tr>
                            <th>Nomor KTP</th>
                            <td>: &nbsp; {{ $data_probing['no_ktp'] }}</td>
                        </tr>
                        <tr>
                            <th>Nomor Handphone</th>
                            <td>: &nbsp; {{ $data_probing['no_hp'] }}</td>
                        </tr>
                        <tr>
                            <th>Koordinat</th>
                            <td>: &nbsp; {{ $data_probing['koordinat_x'] }}, {{ $data_probing['koordinat_y'] }}</td>
                        </tr>
                        <tr>
                            <th>Tanggal Probing</th>
                            <td>: &nbsp; {{ date('d-M-Y', strtotime($data_probing['created_at'])) }}</td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            @if ($data_probing['is_approved'] == 1)
                            <td>: &nbsp; Disetujui</td>
                            @elseif ($data_probing['is_rejected'] == 1)
                            <td>: &nbsp; Ditolak</td>
                            @else
                            <td>: &nbsp; Pending</td>
                            @endif
                        </tr>
                        @if($data_probing['is_rejected'] == 1)
                        <tr>
                            <th>Alasan Ditolak</th>
                            <td>: &nbsp; {{ $data_probing['rejected_reason'] }}</td>
                        </tr>
                        @endif
                        <tr>
                            <th>Nama Pegawai</th>
                            <td>: &nbsp; {{ $data_probing['user']['name'] }}</td>
                        </tr>
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tr>
                            <td colspan="2" class="align-middle text-center">
                                @if($data_probing['is_rejected'] == 0)
                                    <button class="btn btn-danger" onclick="setTidakSesuai()">Reject</button>&nbsp;
                                @endif
                                @if($data_probing['is_approved'] == 0)
                                    <button class="btn btn-primary" onclick="setTerima()">Accept</button>&nbsp;
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <hr>
  </div>
@endsection  
 
@section('afterscript')
<script>
    function setTidakSesuai()
    {
        var p = prompt("Masukan alasan probing ini ditolak");
        if (p != null) {
            if(p == ''){
                alert("Wajib memasukan alasan ditolak");
            }else{
                var r = confirm("Apakah anda yakin akan menolak Probing ini?");
                if (r == true) {

                    var data  = {
                        status: 2,
                        catatan: p
                    };

                    axios.post('{{url('/Approval/Probing/') ."/". $data_probing['id']}}', data)
                    .then(function (response) {
                        if(response.status == 200){
                            alert("Data berhasil disimpan");
                            window.location = "{{ url('/home') }}";
                        }
                    })
                    .catch(function (error) {
                        alert("Data gagal disimpan");
                    });
                }
            }
        }
    }

    function setTerima()
    {
        var data  = {
            status: 1,
        };

        var r = confirm("Apakah anda yakin akan menerima Probing ini?");
        if (r == true) {
            axios.post('{{url('/Approval/Probing/') ."/". $data_probing['id']}}', data)
            .then(function (response) {
                if(response.status == 200){
                    alert("Data berhasil disimpan");
                    window.location = "{{ url('/home') }}";
                }
            })
            .catch(function (error) {
                alert("Data gagal disimpan");
            });
        }
    }
</script>
@stop