@extends('layouts.app')
@section('content')

  <div class="wrapper wrapper-content">
    <div class="row">
        <h2>
            <strong>
                DETAIL PROBING
            </strong>
        </h2>
    </div>

    <div class="row">
        <div class="col-lg-5">
            
            <div class="row">

                <div class="ibox-content">
                    @if (!empty($data_closing['Reward']['path_image']))
                    <div id="gbrLaporanCKR" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#gbrLaporanCKR" data-slide-to="0" class="active"></li>
                        </ol>
                            
                        <div class="carousel-inner">
                            <div class="item active">
                              @if (!empty($data_closing['Reward']['path_image']) && $data_closing['Reward']['path_image'] != null)
                                <a href="{{ url($data_closing['Reward']['path_image']) }}" title="Image from Unsplash" data-gallery="">
                                    <img src="{{ url($data_closing['Reward']['path_image']) }}" alt="">
                                </a>
                                @else
                                <a href="{{ asset('/assets/img/no_photo.jpg') }}" title="No Imave Available" data-gallery="">
                                    <img src="{{ asset('/assets/img/no_photo.jpg') }}" alt="No Image Available">
                                </a>
                              @endif
                                
                            </div>
                        </div>
                        
                        <!-- kontrol-->
                        <a class="carousel-control left" href="#gbrLaporanCKR" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control right" href="#gbrLaporanCKR" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    @else
                    <div id="gbrLaporanCKR" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="item active">
                                <a href="{{ asset('/assets/img/no_photo.jpg') }}" title="No Imave Available" data-gallery="">
                                    <img src="{{ asset('/assets/img/no_photo.jpg') }}" alt="No Image Available">
                                </a>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>


        <div class="col-lg-7">
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tr>
                            <th>Direquest Oleh</th>
                            <td>: &nbsp; {{ $data_closing['User']['name'] }}</td>
                        </tr>
                        <tr>
                            <th>Tanggal Request</th>
                            <td>: &nbsp; {{ date('d-M-Y', strtotime($data_closing['created_at'])) }}</td>
                        </tr>
                        <tr>
                            <th>Point User</th>
                            <td>: &nbsp; {{ number_format($data_closing['jml_point']) }}</td>
                        </tr>
                        <tr>
                            <th>Nama Hadiah</th>
                            <td>: &nbsp; {{ $data_closing['reward']['nama_reward'] }}</td>
                        </tr>
                        <tr>
                            <th>Point Hadiah</th>
                            <td>: &nbsp; {{ number_format($data_closing['reward']['point']) }}</td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            @if ($data_closing['is_approve_point'] == 1)
                            <td>: &nbsp; Disetujui</td>
                            @elseif ($data_closing['is_reject_point'] == 1)
                            <td>: &nbsp; Ditolak</td>
                            @else
                            <td>: &nbsp; Pending</td>
                            @endif
                        </tr>
                        @if ($data_closing['is_reject_point'] == 1)
                        <tr>
                            <th>Alasan Ditolak</th>
                            <td>: &nbsp; {{ $data_closing['rejected_reason'] }}</td>
                        </tr>
                        @endif
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tr>
                            <td colspan="2" class="align-middle text-center">
                                @if($data_closing['is_reject_point'] == 0)
                                    <button class="btn btn-danger" onclick="setTidakSesuai()">Reject</button>&nbsp;
                                @endif
                                @if($data_closing['is_approve_point'] == 0)
                                    <button class="btn btn-primary" onclick="setTerima()">Accept</button>&nbsp;
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <hr>
  </div>
@endsection  
 
@section('afterscript')
<script>
    function setTidakSesuai()
    {
        var p = prompt("Masukan alasan probing ini ditolak");
        if (p != null) {
            if(p == ''){
                alert("Wajib memasukan alasan ditolak");
            }else{
                var r = confirm("Apakah anda yakin akan menolak Probing ini?");
                if (r == true) {

                    var data  = {
                        status: 2,
                        catatan: p
                    };

                    axios.post('{{url('/Approval/Redeem/') ."/". $data_closing['id']}}', data)
                    .then(function (response) {
                        alert("Data berhasil disimpan");
                        window.location = "{{ url('/home') }}";
                    })
                    .catch(function (error) {
                        console.log(error);
                        alert("Data gagal disimpan");
                    });
                }
            }
        }
    }

    function setTerima()
    {
        var data  = {
            status: 1,
        };

        var r = confirm("Apakah anda yakin akan menerima Probing ini?");
        if (r == true) {
            axios.post('{{url('/Approval/Redeem/') ."/". $data_closing['id']}}', data)
            .then(function (response) {
                alert("Data berhasil disimpan");
                window.location = "{{ url('/home') }}";
            })
            .catch(function (error) {
                console.log(error);
                alert("Data gagal disimpan");
            });
        }
    }
</script>
@stop