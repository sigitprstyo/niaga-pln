<div class="modal inmodal" id="modal_Diabaikan" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">FORM VERIFIKASI</h4>
            </div>
            <div class="modal-body">
                <p>
                    <form id="modal_formDiabaikan">
                        <div class="form-group row">
                            <label for="modal_bayar" class="col-lg-3 col-form-label">Tanggal Bayar</label>
                            <div class="col-lg-9">
                                <input type="date" id="modal_bayar" name="modal_bayar" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="modal_tmp" class="col-lg-3 col-form-label">Penanda TMP</label>
                            <div class="col-lg-9">
                                <select id="modal_tmp" name="modal_tmp" class="form-control">
                                    <option value="5">5 Hari</option>
                                    <option value="15">15 Hari</option>
                                    <option value="25">25 Hari</option>
                                    <option value="100">100 Hari</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="modal_remaja" class="col-lg-3 col-form-label">Tanggal Peremajaan</label>
                            <div class="col-lg-9">
                                <input type="date" id="modal_remaja" name="modal_remaja" class="form-control" />
                            </div>
                        </div>
                    </form>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" id="modal_btnVerifikasi" class="btn btn-primary btn-sm">
                    <i class="fa fa-check"></i> KONFIRMASI
                </button>    
            </div>
        </div>
    </div>
</div>
    
    