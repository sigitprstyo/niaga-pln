@extends('layouts.app')
@section('content')

  <div class="wrapper wrapper-content">
    <div class="row">
        <h2>
            <strong>
                DETAIL CLOSING
            </strong>
        </h2>
    </div>

    
    <div class="row">
        <div class="col-lg-5">
            
            <div class="row">

                <div class="ibox-content">
                    @if (count($data_closing['Photo']) > 0)
                    <div id="gbrProbing" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            @foreach ($data_closing['Photo'] as $key => $item)
                                <li data-target="#gbrProbing" data-slide-to="{{ $key }}" class="{{ ($key == 0) ? 'active': '' }}"></li>
                            @endforeach
                        </ol>
                            
                        <div class="carousel-inner">
                            @foreach ($data_closing['Photo'] as $key => $item)
                            <div class="item {{ ($key == 0) ? 'active': '' }}">
                                @if (!empty($item->path_foto) && $item->path_foto != null)
                                <a href="{{ asset($item->path_foto) }}" title="Image from Unsplash" data-gallery="">
                                    <img src="{{ asset($item->path_foto) }}" alt="">
                                </a>
                                @else
                                <a href="{{ asset('/assets/img/no_photo.jpg') }}" title="No Imave Available" data-gallery="">
                                    <img src="{{ asset('/assets/img/no_photo.jpg') }}" alt="No Image Available">
                                </a>
                                @endif
                                
                            </div>
                            @endforeach
                        </div>
                        
                        <!-- kontrol-->
                        <a class="carousel-control left" href="#gbrProbing" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control right" href="#gbrProbing" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    @else
                    <div id="gbrProbing" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="item active">
                                <a href="{{ asset('/assets/img/no_photo.jpg') }}" title="No Imave Available" data-gallery="">
                                    <img src="{{ asset('/assets/img/no_photo.jpg') }}" alt="No Image Available">
                                </a>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>


        <div class="col-lg-7">
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tr>
                            <th>Nama Pelanggan</th>
                            <td>: &nbsp; {{ $data_closing['pelanggan']['nama_pelanggan'] }}</td>
                        </tr>
                        <tr>
                            <th>Nomor KTP</th>
                            <td>: &nbsp; {{ $data_closing['pelanggan']['no_ktp'] }}</td>
                        </tr>
                        <tr>
                            <th>Nomor Handphone</th>
                            <td>: &nbsp; {{ $data_closing['pelanggan']['no_hp'] }}</td>
                        </tr>
                        <tr>
                            <th>Nomor Agenda</th>
                            <td>: &nbsp; {{ $data_closing['no_agenda'] }}</td>
                        </tr>
                        <tr>
                            <th>Koordinat</th>
                            <td>: &nbsp; {{ $data_closing['pelanggan']['koordinat_x'] }}, {{ $data_closing['pelanggan']['koordinat_y'] }}</td>
                        </tr>
                        <tr>
                            <th>Tanggal Closing</th>
                            <td>: &nbsp; {{ date('d-M-Y', strtotime($data_closing['created_at'])) }}</td>
                        </tr>
                        @if($data_closing['tgl_bayar'])
                        <tr>
                            <th>Tanggal Bayar</th>
                            <td>: &nbsp; {{ date('d-M-Y', strtotime($data_closing['tgl_bayar'])) }}</td>
                        </tr>
                        @endif
                        @if($data_closing['tgl_remaja'])
                        <tr>
                            <th>Tanggal Remaja</th>
                            <td>: &nbsp; {{ date('d-M-Y', strtotime($data_closing['tgl_remaja'])) }}</td>
                        </tr>
                        @endif
                        @if($data_closing['nilai_tmp'])
                        <tr>
                            <th>Penanda TMP</th>
                            <td>: &nbsp; {{ $data_closing['nilai_tmp'] }}</td>
                        </tr>
                        @endif
                        <tr>
                            <th>Status</th>
                            @if ($data_closing['is_approved'] == 1)
                            <td>: &nbsp; Disetujui</td>
                            @elseif ($data_closing['is_rejected'] == 1)
                            <td>: &nbsp; Ditolak</td>
                            @else
                            <td>: &nbsp; Pending</td>
                            @endif
                        </tr>
                        @if($data_closing['is_rejected'] == 1)
                        <tr>
                            <th>Alasan Ditolak</th>
                            <td>: &nbsp; {{ $data_closing['rejected_reason'] }}</td>
                        </tr>
                        @endif
                        <tr>
                            <th>Nama Pegawai</th>
                            <td>: &nbsp; {{ $data_closing['user']['name'] }}</td>
                        </tr>
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <tr>
                            <td colspan="2" class="align-middle text-center">
                                @if($data_closing['is_rejected'] == 0)
                                    <button class="btn btn-danger" onclick="setTidakSesuai()">Reject</button>&nbsp;
                                @endif
                                @if($data_closing['is_approved'] == 0)
                                    <button class="btn btn-primary" onclick="setTerima()">Accept</button>&nbsp;
                                @endif
                                <button class="btn btn-success" onclick="setVerifikasi()">Verifikasi</button>&nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <hr>
  </div>

  @include('Transaksi/closing/modal_verifikasi')

@endsection  
 
@section('afterscript')
<script>
    var records = {!! json_encode($data_closing, JSON_HEX_TAG) !!}

    function setTidakSesuai()
    {
        var p = prompt("Masukan alasan Closing ini ditolak");
        if (p != null) {
            if(p == ''){
                alert("Wajib memasukan alasan ditolak");
            }else{
                var r = confirm("Apakah anda yakin akan menolak Closing ini?");
                if (r == true) {

                    var data  = {
                        status: 2,
                        catatan: p
                    };

                    axios.post("{{url('/Approval/Closing/') .'/'. $data_closing['id']}}", data)
                    .then(function (response) {
                        if(response.status == 200){
                            alert("Data berhasil disimpan");
                            window.location = "{{ url('/home') }}";
                        }
                    })
                    .catch(function (error) {
                        alert("Data gagal disimpan");
                    });
                }
            }
        }
    }

    function setTerima()
    {
        var data  = {
            status: 1,
        };

        var r = confirm("Apakah anda yakin akan menerima Probing ini?");
        if (r == true) {
            axios.post("{{url('/Approval/Closing/') .'/'. $data_closing['id']}}", data)
            .then(function (response) {
                if(response.status == 200){
                    alert("Data berhasil disimpan");
                    window.location = "{{ url('/home') }}";
                }
            })
            .catch(function (error) {
                console.log(error);
                alert("Data gagal disimpan");
            });
        }
    }

    /** SHOW MODAL TIDAK SESUAI */
    function setVerifikasi()
    {
        $("#modal_bayar").val('');
        $("#modal_tmp").val('');
        $("#modal_remaja").val('');
        $("#modal_Diabaikan").modal('show');
    }

    $(function () {
        console.log(records);


        /** MODAL BUTTON TIDAK SESUAI DI CLIK */
        $("#modal_btnVerifikasi").click(function (e) { 
            e.preventDefault();
            var id_laporan = records.id;
            var data  = {
                tgl_bayar: $("#modal_bayar").val(),
                tmp: $("#modal_tmp").val(),
                tgl_remaja: $("#modal_remaja").val()
            };

            var r = confirm("Data akan secara otomatis disetujui/ditolak. Apakah anda yakin akan mengverifikasi data ini?");
            if (r == true) {
                axios.post("{{url('/Approval/Verifikasi') .'/'. $data_closing['id']}}", data)
                    .then(function (response) {
                        if(response.status == 200){
                            alert("Data berhasil diverifikasi.");
                            window.location = "{{ url('/home') }}";
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                        alert("Data gagal diverifikasi");
                    });
            }
        });
    });
</script>
@stop