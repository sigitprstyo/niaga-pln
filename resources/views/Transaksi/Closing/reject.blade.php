@extends('layouts.app')
@section('content')


    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif



<div class="panel panel-default panel-border-color panel-border-color-danger">
                <div class="panel-heading panel-heading-divider">Daftar Closing Ditolak</div>
                <div class="panel-body">
    <div class="table-responsive">
    <table class="table table-striped">
    <thead>
      <tr>

        <th>No Agenda</th>
        <th>Nama Pelanggan</th>
        <th>No KTP</th>
        <th>Tarif</th>
        <th>Daya</th>
        <th>Tgl Closing</th>
        <th>Alasan</th>
      </tr>
    </thead>
     <tbody>
      @foreach ($list_point as $key)
      <tr>
        <td>{{ $key->no_agenda }}</td>
        <td>{{ $key->Pelanggan->name }}</td>
        <td>{{ $key->Pelanggan->no_ktp}}</td>
        <td>{{ $key->Pelanggan->tarif }}</td>
        <td>{{ $key->Pelanggan->daya }}</td>
        <td>{{ date('d-M-Y', strtotime($key->created_at)) }}</td>
        <td>{{ $key->rejected_reason }}</td>
      </tr>
      @endforeach
      
    </tbody>
  
  </table>
</div>
</div>
</div>

<div id="modal-form" class="modal fade" aria-hidden="true">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                    <h3 class="m-t-none m-b">Filter By:</h3>
                                                    </div>
                                                    <div class="col-sm-10">

                                                        <form role="form" method="post" action="">
                                                          {{csrf_field()}}
                                                            <div class="form-group" id="data_1">
                                                              <label class="font-normal">Dari Tanggal</label>
                                                                    <div class="input-group date">
                                                                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="from_tanggal" value="{{Carbon\Carbon::today()->format('Y-m-d')}}">
                                                                     </div>
                                                                </div>
                                                                <div class="form-group" id="data_1">
                                                              <label class="font-normal">Ke Tanggal</label>
                                                                    <div class="input-group date">
                                                                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="to_tanggal" value="{{Carbon\Carbon::today()->format('Y-m-d')}}">
                                                                     </div>
                                                                </div>
                                                            <div>
                                                                <input class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit" name="filter" value="Filter">
                                                                <label> 
                                                            </div>
                                                        </form>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                            </div>

  <script>
    $(".delete").on("submit", function(){
        return confirm("Do you want to delete this item?");
    });
</script>
 @endsection   