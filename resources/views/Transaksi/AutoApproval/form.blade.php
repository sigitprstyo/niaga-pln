@extends('layouts.app')
@section('content')

  @if ($errors->any())
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div><br />
  @endif
  @if (\Session::has('success'))
  <div class="alert alert-success">
      <p>{{ \Session::get('success') }}</p>
  </div><br />
  @endif

  <div class="panel panel-default panel-border-color panel-border-color-primary">
    <div class="panel-heading panel-heading-divider">Upload Data AP2T</div>
    <div class="panel-body">

      <div class="row m-t-sm">
        <div class="col-lg-12">
          <div class="panel blank-panel">

            <div class="panel-body">

              <div class="tab-content">
                <div class="tab-pane active" id="tab-1">

                  <form method="post" action="{{url('Approval/AutoApproval')}}" enctype="multipart/form-data">
                    {{csrf_field()}}

                    <div class="row">
                      <div class="col-md-6">
                        <button type="button" id="btnUpload" class="btn btn-block btn-primary pull-left" style="width: auto;">Upload Data</button>
                      </div>
                      <div class="col-md-6">
                        <button type="button" id="btnTemplate" class="btn btn-block btn-primary pull-right" style="width: auto;">Download Template</button>
                      </div>
                      <input id='fileUpload' type='file' style="display:none;"/>
                      <input id='datajson' type='hidden' name="datajson"/>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col-md-12 table-responsive">
                        <table class="tabel table table-bordered" id="tbl_spot_penyulang">
                          <thead>
                            <tr>
                              <th>No Agenda</th>
                              <th>Nama Pelanggan</th>
                              <th>No KTP</th>
                              <th>Tgl Bayar</th>
                              <th>Penanda TMP</th>
                              <th>Tgl Peremajaan</th>
                            </tr>
                          </thead>
                          <tbody>                        
                          </tbody>
                        </table>
                      </div>
                      <!-- /.col -->
                    </div>

                    <div class="row">
                      <div class="col-lg-12 margin-tb"></div>
                      <div class="form-group col-md-4">
                        <button type="submit" class="btn btn-primary btn-sm" style="margin-left:18px">Submit</button>
                      </div>
                    </div>
                  </form>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection  

@section('afterscript')
  <script src="{{ asset('js/FileSaver.min.js') }}" type="text/javascript"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.15.1/xlsx.full.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>

  <script type="text/javascript">

    $(document).ready(function(){

      var X = XLSX;
      var fileUpload = document.getElementById('fileUpload');
      var btnUpload = document.getElementById('btnUpload');
      var btnTemplate = document.getElementById('btnTemplate');
      
      var wb = XLSX.utils.book_new();
      wb.SheetNames.push("Test Sheet");

      var ws_data = [['No_Agenda', 'Nama_Pelanggan', 'No_KTP', 'Tgl_Bayar', 'Penanda_TMP', 'Tgl_Peremajaan']];
      var ws = XLSX.utils.aoa_to_sheet(ws_data);
      wb.Sheets["Test Sheet"] = ws;

      var wbout = XLSX.write(wb, {bookType:'xlsx',  type: 'binary'});
      
      function uploadFile(e) {
        var files = e.target.files;
        var f = files[0];
        {
            var reader = new FileReader();
            var name = f.name;
            reader.onload = function (e) {
                var data = e.target.result;
                var workbook = XLSX.read(data, { type: 'binary', cellDates: true });
                var result = {};
                workbook.SheetNames.forEach(function (sheetName) {
                    var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                    if (roa.length > 0) {
                        console.log(sheetName, roa);
                        result[sheetName] = roa;
                    }
                });
                var output_str = JSON.stringify(result, 2, 2);
                var output_arr = JSON.parse(output_str);
                var newtable = output_arr["Test Sheet"].map(function callback(value, id) {
                                    return "<tr><td>" + value.No_Agenda + "</td><td>" + value.Nama_Pelanggan +
                                          "</td><td>" + (value.No_KTP || "") + "</td><td>" + 
                                          moment(value.Tgl_Bayar).format('DD-MMM-YYYY') + "</td><td>" + 
                                          value.Penanda_TMP + "</td><td>" + 
                                          moment(value.Tgl_Peremajaan).format('DD-MMM-YYYY') + "</td></tr>" ;
                                });
                $("#datajson").val(JSON.stringify(output_arr["Test Sheet"]));

                $("#tbl_spot_penyulang tbody").html(newtable);
            }
            reader.readAsBinaryString(f);
        }
      }

      function openDialog() {
        document.getElementById('fileUpload').click();
      }

      function generateTemplate(e) {
        saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), 'test.xlsx');
      }

      function s2ab(s) { 
        var buf = new ArrayBuffer(s.length); //convert s to arrayBuffer
        var view = new Uint8Array(buf);  //create uint8array as viewer
        for (var i=0; i<s.length; i++) view[i] = s.charCodeAt(i) & 0xFF; //convert to octet
        return buf;
      }

      if (fileUpload.addEventListener)
          fileUpload.addEventListener('change', uploadFile, false);

      if (btnUpload.addEventListener)
          btnUpload.addEventListener('click', openDialog);

      if (btnTemplate.addEventListener)
          btnTemplate.addEventListener('click', generateTemplate);

      $('#tbl_spot_penyulang').DataTable({
        "paging": true,
        "searching": false
      });

    });
    
  </script>
@endsection