<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@yield('title','MARKETER NIAGA - PLN WILAYAH SUMATERA UTARA')</title>

  <link rel="stylesheet" type="text/css" href="{{ asset('beagle/lib/perfect-scrollbar/css/perfect-scrollbar.min.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('beagle/lib/material-design-icons/css/material-design-iconic-font.min.css') }}" />
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <link rel="stylesheet" href="{{ asset('beagle/css/style.css') }}" type="text/css" />
  <!-- <link rel="stylesheet" type="text/css" href="{{ asset('beagle/lib/summernote/summernote.css') }}"/> -->
  <link rel="stylesheet" href="{{ asset('beagle/lib/datapicker/datepicker3.css') }}" type="text/css" />
  <link rel="stylesheet" type="text/css" href="{{ asset('beagle/lib/dataTables2/datatables.min.css') }}" />

  @yield('afterhead')
</head>

<body>
  <div class="be-wrapper be-fixed-sidebar be-color-header-danger">
    <nav class="navbar navbar-default navbar-fixed-top be-top-header">
      <div class="container-fluid">

        <div class="be-right-navbar">
          <ul class="nav navbar-nav navbar-right be-user-nav">
            <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><img src="{{ asset('beagle/img/user.png') }}" style="max-height: 30px;" alt="Avatar"></a>
              <ul role="menu" class="dropdown-menu">
                @guest
                <li><a href="{{ route('register') }}"><span class="icon mdi mdi-face"></span>Sign Up</a></li>
                @else
                <li>
                  <div class="user-info">
                    <div class="user-name">{{ Auth::user()->name }}</div>
                    <div class="user-position">{{ Auth::user()->RoleUser->role->name }}</div>
                  </div>
                </li>
                <!--                   <li><a href="#"><span class="icon mdi mdi-face"></span> Account</a></li>
                  <li><a href="#"><span class="icon mdi mdi-settings"></span> Settings</a></li> -->
                <li><a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span class="icon mdi mdi-power"></span> Logout</a></li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form>
                @endguest
              </ul>
            </li>
          </ul>
          <div class="page-title"><span style="color: white;font-weight: bold;">PLN MARKETER NIAGA</span></div>
          @guest
          @else
          <ul class="nav navbar-nav navbar-right be-icons-nav">
            <li class="dropdown"><a><img src="{{ asset('beagle/img/Logo_PLN.png') }}" style="max-height: 50px;"></a>
            </li>

          </ul>
          @endguest
        </div>
      </div>
    </nav>
    @guest
    @else
    <div class="be-left-sidebar">
      <div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle">Dashboard</a>
        <div class="left-sidebar-spacer">
          <div class="left-sidebar-scroll">
            <div class="left-sidebar-content">
              <ul class="sidebar-elements">
                <li class="divider">Menu</li>
                <li class="active">
                  <a href="{{ route('home') }}">
                    <i class="icon mdi mdi-home"></i><span>Dashboard</span>
                  </a>
                </li>
                @if (Auth::user()->isRole('administrator'))
                <li class="parent"><a href="#"><i class="icon mdi mdi-grid"></i><span>Master</span></a>
                  <ul class="sub-menu">
                    <li><a href="{{ url ('/Master/Reward') }}">Reward</a></li>
                    <li><a href="{{ url ('/Master/Tarif') }}">Tarif</a></li>
                    <li><a href="{{ url ('/Master/Daya') }}">Daya</a></li>
                    <li><a href="{{ url ('/Master/Iklan') }}">Iklan</a></li>
                    <li><a href="{{ url ('/Master/User') }}">User</a></li>
                  </ul>
                </li>
                <li class="parent"><a href="#"><i class="icon mdi mdi-assignment-check"></i><span>Approval</span></a>
                  <ul class="sub-menu">
                    {{-- <li><a href="{{ url ('/Approval/AutoApproval') }}">Auto Approval</a></li> --}}
                    {{-- <li><a href="{{ url ('/Approval/Probing') }}">Probing</a></li> --}}
                    <li class="parent"><a href="#"><span>Probing</span></a>
                      <ul class="sub-menu">
                        <li><a href="{{ url ('/Approval/Probing') }}">Pending</a></li>
                        <li><a href="{{ url ('/Approval/ProbingReject') }}">Rejected</a></li>
                      </ul>
                    </li>
                    {{-- <li><a href="{{ url ('/Approval/Closing') }}">Closing</a></li> --}}
                    <li class="parent"><a href="#"><span>Closing</span></a>
                      <ul class="sub-menu">
                        <li><a href="{{ url ('/Approval/Closing') }}">Pending</a></li>
                        <li><a href="{{ url ('/Approval/ClosingReject') }}">Rejected</a></li>
                      </ul>
                    </li>
                    {{-- <li><a href="{{ url ('/Approval/Redeem') }}">Redeem Reward</a></li> --}}
                    <li class="parent"><a href="#"><span>Redeem Reward</span></a>
                      <ul class="sub-menu">
                        <li><a href="{{ url ('/Approval/Redeem') }}">Pending</a></li>
                        <li><a href="{{ url ('/Approval/RedeemReject') }}">Rejected</a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li class="parent"><a href="#"><i class="icon mdi mdi-assignment"></i><span>Rekap</span></a>
                  <ul class="sub-menu">
                    <li><a href="{{ url ('/Rekap/PasangBaru') }}">Pasang Baru</a></li>
                    <li><a href="{{ url ('/Rekap/PerubahanDaya') }}">Perubahan Daya</a></li>
                    <li><a href="{{ url ('/Rekap/ProbingClosing') }}">Probing vs Closing</a></li>
                    <li><a href="{{ url ('/Rekap/Pending') }}">Pending</a></li>
                    <li><a href="{{ url ('/Rekap/Submitted') }}">Submitted</a></li>
                    <li><a href="{{ url ('/Rekap/Completed') }}">Completed</a></li>
                    <li><a href="{{ url ('/Rekap/Rejected') }}">Rejected</a></li>
                  </ul>
                </li>
                @endif
                @if (Auth::user()->isRole('administrator'))
                @endif

              </ul>
              </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="be-content">
      @endguest
      <!-- <div class="main-content container-fluid"></div> -->

      <div class="main-content">
        @yield('content')
      </div>

    </div>

  </div>
  @yield('beforescript')
  <script src="{{ asset('beagle/lib/jquery/jquery.min.js') }}" type="text/javascript"></script>

  <!-- <script src="{{ asset('beagle/lib/summernote/jquery.js') }}"></script>  -->

  <script src="{{ asset('beagle/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('beagle/js/main.js') }}" type="text/javascript"></script>
  <script src="{{ asset('beagle/lib/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('beagle/lib/datapicker/bootstrap-datepicker.js') }}"></script>

  <!-- <script src="{{ asset('beagle/lib/jquery-flot/jquery.flot.js') }}" type="text/javascript"></script> -->
  <!-- <script src="{{ asset('beagle/lib/jquery-flot/plugins/curvedLines.js') }}" type="text/javascript"></script> -->
  <script src="{{ asset('beagle/lib/jquery.sparkline/jquery.sparkline.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('beagle/lib/countup/countUp.min.js') }}" type="text/javascript"></script>
  <!-- <script src="{{ asset('beagle/js/app-dashboard.js') }}" type="text/javascript"></script> -->
  <script src="{{ asset('beagle/lib/chartjs/Chart.min.js') }}" type="text/javascript"></script>
  <!-- <script src="{{ asset('beagle/js/app-charts-chartjs.js') }}" type="text/javascript"></script> -->
  <!-- include libraries(jQuery, bootstrap) -->

  <!-- <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>  -->

  <!-- include summernote css/js -->
  <!-- <link href="{{ asset('beagle/lib/summernote/summernote.css') }}" rel="stylesheet">
    <script src="{{ asset('beagle/lib/summernote/summernote.js') }}"></script> -->

  <script src="{{ asset('beagle/lib/dataTables2/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('beagle/lib/datatables/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('beagle/lib/datatables/plugins/buttons/js/dataTables.buttons.js') }}" type="text/javascript"></script>
  <script src="{{ asset('beagle/lib/datatables/plugins/buttons/js/buttons.html5.js') }}" type="text/javascript"></script>
  <script src="{{ asset('beagle/lib/datatables/plugins/buttons/js/buttons.flash.js') }}" type="text/javascript"></script>
  <script src="{{ asset('beagle/lib/datatables/plugins/buttons/js/buttons.print.js') }}" type="text/javascript"></script>
  <script src="{{ asset('beagle/lib/datatables/plugins/buttons/js/buttons.colVis.js') }}" type="text/javascript"></script>
  <script src="{{ asset('beagle/lib/datatables/plugins/buttons/js/buttons.bootstrap.js') }}" type="text/javascript"></script>


  <script src="{{ asset('beagle/lib/jquery-flot/jquery.flot.js') }}" type="text/javascript"></script>
  <script src="{{ asset('beagle/lib/jquery-flot/plugins/curvedLines.js') }}" type="text/javascript"></script>
  <script src="{{ asset('beagle/lib/jquery.sparkline/jquery.sparkline.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('beagle/lib/countup/countUp.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('beagle/js/app-dashboard.js') }}" type="text/javascript"></script>

  <script src="{{ asset('beagle/lib/raphael/raphael-min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('beagle/lib/morrisjs/morris.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('beagle/js/app-charts-morris.js') }}" type="text/javascript"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

  <script type="text/javascript">
    $('#data_1 .input-group.date').datepicker({
      todayBtn: "linked",
      format: 'yyyy-mm-dd',
      keyboardNavigation: false,
      forceParse: false,
      calendarWeeks: true,
      autoclose: true
    });

    $("#table3").dataTable({
      "aaSorting": [],
      buttons: [{
          extend: 'excel',
          title: 'PLN Niaga Data'
        },
        {
          extend: 'pdf',
          title: 'PLN Niaga Data',
          orientation: 'landscape',
          pageSize: 'TABLOID'
        },
        {
          extend: 'print',
          customize: function(win) {
            $(win.document.body).addClass('white-bg');
            $(win.document.body).css('font-size', '8px');

            $(win.document.body).find('table')
              .addClass('compact')
              .css('font-size', 'inherit');
          }
        }
      ],
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      dom: "<'row'<'col-sm-5'l><'col-sm-3 text-right'f><'col-sm-4 text-right'B>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>"
    });

    $(document).ready(function() {
      //initialize the javascript
      App.init();
      // App.dashboard();
      // App.chartsMorris();
      // App.ChartJs();
    });
  </script>
  @yield('afterscript')
</body>

</html>