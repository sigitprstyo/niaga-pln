@extends('layouts.app')
@section('content')

@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div><br />
@endif
@if (\Session::has('success'))
<div class="alert alert-success">
  <p>{{ \Session::get('success') }}</p>
</div><br />
@endif

<div class="panel panel-default panel-border-color panel-border-color-primary">
  <div class="panel-body">

    <div class="row m-t-sm">
      <div class="col-lg-12">
        <div class="panel blank-panel">
          <div class="panel-heading">
            <div class="panel-options">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1" data-toggle="tab">Edit</a></li>
              </ul>
            </div>
          </div>

          <div class="panel-body">

            <div class="tab-content">
              <div class="tab-pane active" id="tab-1">

                <form method="post" action="{{action('Master\KelompokTarifController@update', $kelompoktarif->id )}}">
                  {{csrf_field()}}

                  <div class="row">
                    <div class="col-lg-12 margin-tb"></div>
                    <div class="form-group col-md-8">
                      <label for="code">Code:</label>
                      <input type="text" name="code" class="form-control" value="{{ $kelompoktarif->code }}">
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-12 margin-tb"></div>
                    <div class="form-group col-md-8">
                      <label for="name">Nama:</label>
                      <input type="text" name="name" class="form-control" value="{{ $kelompoktarif->name }}">
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-12 margin-tb"></div>
                    <div class="form-group col-md-8">
                      <label for="point_probing_pb">Point Probing PB:</label>
                      <input type="number" name="point_probing_pb" class="form-control" value="{{ $kelompoktarif->point_probing_pb }}">
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-12 margin-tb"></div>
                    <div class="form-group col-md-8">
                      <label for="point_closing_pb">Point Closing PB:</label>
                      <input type="number" name="point_closing_pb" class="form-control" value="{{ $kelompoktarif->point_closing_pb }}">
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-12 margin-tb"></div>
                    <div class="form-group col-md-8">
                      <label for="point_probing_pd">Point Probing PD:</label>
                      <input type="number" name="point_probing_pd" class="form-control" value="{{ $kelompoktarif->point_probing_pd }}">
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-12 margin-tb"></div>
                    <div class="form-group col-md-8">
                      <label for="point_closing_pd">Point Closing PD:</label>
                      <input type="number" name="point_closing_pd" class="form-control" value="{{ $kelompoktarif->point_closing_pd }}">
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-12 margin-tb"></div>
                    <div class="form-group col-md-4">
                      <button type="submit" class="btn btn-primary btn-sm" style="margin-left:18px">Submit</button>
                    </div>
                  </div>
                </form>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection