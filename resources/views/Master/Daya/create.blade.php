@extends('layouts.app')
@section('content')

  @if ($errors->any())
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div><br />
  @endif
  @if (\Session::has('success'))
  <div class="alert alert-success">
      <p>{{ \Session::get('success') }}</p>
  </div><br />
  @endif

  <div class="panel panel-default panel-border-color panel-border-color-primary">
    <div class="panel-heading panel-heading-divider">Tambah Daya</div>
    <div class="panel-body">

      <div class="row m-t-sm">
        <div class="col-lg-12">
          <div class="panel blank-panel">

            <div class="panel-body">

              <div class="tab-content">
                <div class="tab-pane active" id="tab-1">

                  <form method="post" action="{{url('Master/Daya')}}">
                    {{csrf_field()}}
                    
                    <div class="row">
                      <div class="col-lg-12 margin-tb"></div>
                      <div class="form-group col-md-8">
                        <label for="nama_reward">Daya:</label>
                        <input type="text" name="daya" class="form-control">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-12 margin-tb"></div>
                      <div class="form-group col-md-4">
                        <button type="submit" class="btn btn-primary btn-sm" style="margin-left:18px">Submit</button>
                      </div>
                    </div>
                  </form>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection  
