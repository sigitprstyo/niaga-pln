@extends('layouts.app')
@section('content')

      @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div><br />
      @endif
      @if (\Session::has('success'))
      <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p>
      </div><br />
      @endif

      <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">Tambah User</div>
                <div class="panel-body">

      <div class="row m-t-sm">
      <div class="col-lg-12">
      <div class="panel blank-panel">

      <div class="panel-body">

      <div class="tab-content">
      <div class="tab-pane active" id="tab-1">

      <form method="post" action="{{url('Master/User')}}">
        {{csrf_field()}}
        
        <div class="row">
          <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-8">
            <label for="nama_penyulang">Nama:</label>
            <input type="text" name="name" class="form-control">
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-8">
            <label for="nama_penyulang">Jabatan:</label>
            <input type="text" name="jabatan" class="form-control">
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-8">
            <label for="nama_penyulang">Email:</label>
            <input type="text" name="email" class="form-control">
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-8">
            <label for="nama_penyulang">Password:</label>
            <input type="password" name="password" class="form-control">
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-8">
            <label for="nama_penyulang">Konfirmasi Password:</label>
            <input id="password-confirm" type="password" name="password_confirmation" class="form-control">
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-8">
            <label for="nama_penyulang">Role:</label>
            <select type="text" class="form-control" name="role">
              @foreach ($role as $key)
                <option value="{{ $key->id }}">{{ $key->name }}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="row">
           <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-4">
            <button type="submit" class="btn btn-primary btn-sm" style="margin-left:18px">Submit</button>
          </div>
        </div>
      </form>

      </div>
        </div>
        </div>
        </div>
          </div>
        </div>
      </div>
@endsection  
