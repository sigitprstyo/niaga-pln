@extends('layouts.app')
@section('content')

@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div><br />
@endif
@if (\Session::has('success'))
<div class="alert alert-success">
  <p>{{ \Session::get('success') }}</p>
</div><br />
@endif

<div class="panel panel-default panel-border-color panel-border-color-primary">
  <div class="panel-body">

    <div class="row m-t-sm">
      <div class="col-lg-12">
        <div class="panel blank-panel">
          <div class="panel-heading">
            <div class="panel-options">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-1" data-toggle="tab">Edit</a></li>
              </ul>
            </div>
          </div>

          <div class="panel-body">

            <div class="tab-content">
              <div class="tab-pane active" id="tab-1">

                <form method="post" action="{{action('Master\TarifController@update', $user->id )}}">
                  {{csrf_field()}}

                  <div class="row">
                    <div class="col-lg-12 margin-tb"></div>
                    <div class="form-group col-md-8">
                      <label for="nama">Tipe:</label>
                      <input type="text" name="tipe" class="form-control" value="{{ $user->tipe }}">
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-12 margin-tb"></div>
                    <div class="form-group col-md-8">
                      <label for="jabatan">Harga:</label>
                      <input type="text" name="harga" class="form-control" value="{{ $user->harga }}">
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-12 margin-tb"></div>
                    <div class="form-group col-md-4">
                      <button type="submit" class="btn btn-primary btn-sm" style="margin-left:18px">Submit</button>
                    </div>
                  </div>
                </form>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection