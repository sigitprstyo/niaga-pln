@extends('layouts.app')
@section('content')


    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif

<div class="row">
 <div class="col-lg-12 margin-tb"></div>
 <div class="form-group col-md-1">
<div class="nav navbar-right">
   <a href="{{ url ('/Master/Reward/create') }}" class="btn btn-success btn-sm">+ Tambah</a>
</div>
</div>

<!-- <div class="col-md-1 pull-right">
    <a data-toggle="modal" class="btn btn-sm btn-primary" href="#modal-form">Filter</a>
</div> -->
</div>

<div class="panel panel-default panel-border-color panel-border-color-danger">
                <div class="panel-heading panel-heading-divider">Master Reward</div>
                <div class="panel-body">
    <div class="table-responsive">
    <table class="table table-striped">
    <thead>
      <tr>

        <th>Title</th>
        <th>Point</th>
        <th>Kategory</th>
        <th></th>
        <th></th>
      </tr>
    </thead>
     <tbody>
      @foreach ($reward as $key)
      <tr>
        <td>{{ $key->nama_reward }}</td>
        <td>{{ $key->point}}</td>
        @if ($key->category == 1)
        <td>Hadiah Utama</td>
        @elseif ($key->category == 2)
        <td>Hadiah Kedua</td>
        @elseif ($key->category == 3)
        <td>Hadiah Ketiga</td>
        @elseif ($key->category == 4)
        <td>Hadiah Keempat</td>
        @else
        <td>Hadiah Kelima</td>
        @endif
        <td>   <a href="{{action('Master\RewardController@edit', $key->id )}}" title="Edit"><span class="icon mdi mdi-edit"></span></a>
        </td>
        <td>
          <form action="{{action('Master\RewardController@destroy', $key->id )}}" method="post">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <button class="icon mdi mdi-delete" style="background: transparent;border:none;color: #4285f4;"  title="Delete" onclick="return confirm('Apakah Anda Ingin Hapus..');" class="icon"></button>
          </form>
        </td>
      </tr>
      @endforeach
      
    </tbody>
  
  </table>
</div>
</div>
</div>

<div id="modal-form" class="modal fade" aria-hidden="true">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                    <h3 class="m-t-none m-b">Filter By:</h3>
                                                    </div>
                                                    <div class="col-sm-10">

                                                        <form role="form" method="post" action="">
                                                          {{csrf_field()}}
                                                            <div class="form-group" id="data_1">
                                                              <label class="font-normal">Dari Tanggal</label>
                                                                    <div class="input-group date">
                                                                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="from_tanggal" value="{{Carbon\Carbon::today()->format('Y-m-d')}}">
                                                                     </div>
                                                                </div>
                                                                <div class="form-group" id="data_1">
                                                              <label class="font-normal">Ke Tanggal</label>
                                                                    <div class="input-group date">
                                                                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="to_tanggal" value="{{Carbon\Carbon::today()->format('Y-m-d')}}">
                                                                     </div>
                                                                </div>
                                                            <div>
                                                                <input class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit" name="filter" value="Filter">
                                                                <label> 
                                                            </div>
                                                        </form>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                            </div>

  <script>
    $(".delete").on("submit", function(){
        return confirm("Do you want to delete this item?");
    });
</script>
 @endsection   