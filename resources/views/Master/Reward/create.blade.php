@extends('layouts.app')
@section('content')

  @if ($errors->any())
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div><br />
  @endif
  @if (\Session::has('success'))
  <div class="alert alert-success">
      <p>{{ \Session::get('success') }}</p>
  </div><br />
  @endif

  <div class="panel panel-default panel-border-color panel-border-color-primary">
    <div class="panel-heading panel-heading-divider">Tambah Reward</div>
    <div class="panel-body">

      <div class="row m-t-sm">
        <div class="col-lg-12">
          <div class="panel blank-panel">

            <div class="panel-body">

              <div class="tab-content">
                <div class="tab-pane active" id="tab-1">

                  <form method="post" action="{{url('Master/Reward')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    
                    <div class="row">
                      <div class="col-lg-12 margin-tb"></div>
                      <div class="form-group col-md-8">
                        <label for="nama_reward">Nama:</label>
                        <input type="text" name="nama_reward" class="form-control">
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-lg-12 margin-tb"></div>
                      <div class="form-group col-md-8">
                        <label for="deskripsi">Keterangan:</label>
                        <input type="text" name="deskripsi" class="form-control">
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-lg-12 margin-tb"></div>
                      <div class="form-group col-md-8">
                        <label for="point">Poin:</label>
                        <input type="number" name="point" class="form-control">
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-lg-12 margin-tb"></div>
                      <div class="form-group col-md-8">
                        <label for="category">Kategori:</label>
                        <select type="text" class="form-control" name="category">
                          <option value="1">Hadiah Utama</option>
                          <option value="2">Hadiah Kedua</option>
                          <option value="3">Hadiah Ketiga</option>
                          <option value="4">Hadiah Keempat</option>
                          <option value="5">Hadiah Kelima</option>
                        </select>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-lg-12 margin-tb"></div>
                      <div class="form-group col-md-8">
                        <label for="image">Gambar:</label>
                        <input type="file" name="image" class="form-control">
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-lg-12 margin-tb"></div>
                      <div class="form-group col-md-8">
                        <label for="aktif">Aktif:</label>
                        <input type="checkbox" name="aktif" value="1">
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-lg-12 margin-tb"></div>
                      <div class="form-group col-md-4">
                        <button type="submit" class="btn btn-primary btn-sm" style="margin-left:18px">Submit</button>
                      </div>
                    </div>
                  </form>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection  
