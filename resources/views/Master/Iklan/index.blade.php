@extends('layouts.app')
@section('content')


@if (\Session::has('success'))
<div class="alert alert-success">
  <p>{{ \Session::get('success') }}</p>
</div><br />
@endif

<div class="row">
  <div class="col-lg-12 margin-tb"></div>
  <div class="form-group col-md-1">
    <div class="nav navbar-right">
      <a href="{{ url ('/Master/Iklan/create') }}" class="btn btn-success btn-sm">+ Tambah</a>
    </div>
  </div>

  <!-- <div class="col-md-1 pull-right">
    <a data-toggle="modal" class="btn btn-sm btn-primary" href="#modal-form">Filter</a>
</div> -->
</div>

<div class="panel panel-default panel-border-color panel-border-color-danger">
  <div class="panel-heading panel-heading-divider">Master Iklan</div>
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>

            <th>Tipe</th>
            <th>Keterangan</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($iklan as $key)
          <tr>
            <td>{{ $key->tipe }}</td>
            <td>{{ $key->harga}}</td>
            <td> <a href="{{action('Master\IklanController@edit', $key->id )}}" title="Edit"><span class="icon mdi mdi-edit"></span></a>
            </td>
            <td>
              <form action="{{action('Master\IklanController@destroy', $key->id )}}" method="post">
                {{csrf_field()}}
                <input name="_method" type="hidden" value="DELETE">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <button class="icon mdi mdi-delete" style="background: transparent;border:none;color: #4285f4;" title="Delete" onclick="return confirm('Apakah Anda Ingin Hapus..');" class="icon"></button>
              </form>
            </td>
          </tr>
          @endforeach

        </tbody>

      </table>
    </div>
  </div>
</div>
<script>
  $(".delete").on("submit", function() {
    return confirm("Do you want to delete this item?");
  });
</script>
@endsection