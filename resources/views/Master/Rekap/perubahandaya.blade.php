@extends('layouts.app')
@section('content')


    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif

<div class="row">
  <div class="col-lg-12 margin-tb"></div>

  <div class="col-md-1 pull-right">
      <a data-toggle="modal" class="btn btn-sm btn-primary" href="#modal-form">Filter</a>
  </div>
</div>

<div class="panel panel-default panel-border-color panel-border-color-danger">
                <div class="panel-heading panel-heading-divider">Rekap Perubahan Daya</div>
                <div class="panel-body">
    <div class="table-responsive">
    <table id="table3" class="table table-striped">
    <thead>
      <tr>
        <th>#</th>
        <th>Nama Pegawai</th>
        <th>Pelanggan</th>
        <th>Selisih Daya</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($listdata as $key => $value)
        <tr>
          <td>{{ $key + 1}}</td>
          <td>{{ $value->name }}</td>
          <td>{{ $value->jml_pelanggan}}</td>
          <td>{{ $value->total_daya}}</td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
</div>
</div>
 @endsection