@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-xs-12 col-md-6 col-lg-3">
    <div class="widget widget-tile">

      <div class="data-info">
        <div class="desc">Approval Kredit</div>
        <div class="value"><span data-toggle="counter" data-end="{{$total_approved}}" class="number">{{$total_approved}}</span>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-md-6 col-lg-3">
    <div class="widget widget-tile">

      <div class="data-info">
        <div class="desc">Submited Credit</div>
        <div class="value"><span data-toggle="counter" data-end="{{$total_submitted}}" class="number">{{$total_submitted}}</span>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-md-6 col-lg-3">
    <div class="widget widget-tile">

      <div class="data-info">
        <div class="desc">Rejected Credit</div>
        <div class="value"><span data-toggle="counter" data-end="{{$total_reject}}" class="number">{{$total_reject}}</span>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-md-6 col-lg-3">
    <div class="widget widget-tile">

      <div class="data-info">
        <div class="desc">Completed Credit</div>
        <div class="value"><span data-toggle="counter" data-end="{{$jml_complete}}" class="number">{{$jml_complete}}</span>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="panel panel-default panel-border-color panel-border-color-danger">
  <div class="panel-heading panel-heading-divider"></div>
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th rowspan="2">#</th>
            <th rowspan="2">Nama Pegawai</th>
            <th colspan="2">Probing</th>
            <th colspan="2">Closing</th>
          </tr>
          <tr>
            <th>Pelanggan</th>
            <th>Daya</th>
            <th>Pelanggan</th>
            <th>Daya</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($listdata as $key => $value)
            <tr>
              <td>{{ $key + 1}}</td>
              <td>{{ $value->name }}</td>
              <td>{{ $value->jml_probing}}</td>
              <td>0</td>
              <td>{{ $value->jml_closing}}</td>
              <td>0</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

@section('afterscript')
  <script type="text/javascript">
    $(document).ready(function() {
      //initialize the javascript
      // App.init();
      App.dashboard();
      // App.chartsMorris();
      // App.ChartJs();
    });
  </script>
@endsection